<?php
	#session_start();
	require '../../dbconn.php';
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.css" />
    <script>
                 $(document).ready(function() {
                         $("#users").change(function() {
                                 var user_id = $(this).val();
                                 if(user_id != "") {
                                         $.ajax({
                                                 url: "viewForm.php",
                                                 data: {u_id:user_id},
                                                 type: 'POST',
                                                 success: function(response) {
                                                                 $("#proc").html(response);
                                         }});
                                 } else {
                                         $("#proc").html("<option value=''>---- Select Procedure ----</option>");
                                 }
                         });
                 });
    </script>

    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-info justify-content-center py-0">

      <a class="navbar-brand"><em>Viewing Form</em></a> <!-- make this text light -->

    </nav>

    <div class="jumbotron jumbotron-fluid ">
  <div class="container">
    <h1 class="text-center">Form Info:</h1>
    <form class="" action="formList.php" method="post">

      <div class="container p-2 rounded shadow">
        <div class="row ">
          <div class="col-sm-6">
            <div class="input-group input-group-lg">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-lg">Student Name</span>
              </div>
              <select name="users" id="users" class="custom-select form-control">
		<option value=''>-------- Select Student --------</option>
		    <?php
			$getStudents = mysqli_query($conn, "SELECT CONCAT(FirstName, ' ', LastName) AS 'Name' FROM Student");

			while($getStudentsRow = mysqli_fetch_array($getStudents)) {
				echo "<option value='".$getStudentsRow['Name']."'>".$getStudentsRow['Name']."</option>";
			}
		    ?>
	      </select>
	    </div>
          </div>
          <div class="col-sm-6">
            <div class="input-group input-group-lg">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-lg">Evaluation</span>
              </div>
              <select name="procedure" id="proc" class="custom-select form-control">
                <option value=''>---- Select Procedure ----</option>
              </select>
            </div>
          </div>
        </div>
        <br>
        <div class="row">

          <div class="col-sm-4">
            <hr>
          </div>
          <div class="col-sm-4 ">
            <button type="submit" class="btn btn-primary btn-block btn-lg">Continue</button>
            <!-- add action to go to new page -->
          </div>
          <div class="col-sm-4">
            <hr>
          </div>
        </div>

      </div>
      <div class="row">

      </div>
    </form>
  </div>
</div>




<?php
	if(isset($_POST['u_id'])) {
		$getStudID = mysqli_query($conn, "SELECT StudentID FROM Student WHERE CONCAT(FirstName, ' ', LastName) = '".$_POST['u_id']."'");
		$getStudIDRow = mysqli_fetch_array($getStudID);
		$StudID = $getStudIDRow['StudentID'];
		$res = mysqli_query($conn, "SELECT ProcedureName FROM Proc WHERE ProcedureID IN (SELECT ProcID FROM GreenForm WHERE StudID = '$StudID')");
		if(mysqli_num_rows($res) > 0) {
			while($row = mysqli_fetch_array($res)) {
				echo '<option value="'.$row['ProcedureName'].'">'.$row['ProcedureName'].'</option>';
			}
		} else {
			echo '<option value="">No Record</option>';
	}
	}
?>

  </body>
</html>



