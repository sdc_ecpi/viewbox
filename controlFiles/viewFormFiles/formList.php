<?php
	require '../../dbconn.php';
	$user = $_POST['users'];
	$procedures = $_POST['procedure'];
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.css" />

    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-info justify-content-center py-0">

      <a class="navbar-brand"><em>Viewing Form</em></a> <!-- make this text light -->

    </nav>

    <div class="jumbotron jumbotron-fluid shadow">
     <div class="container">
    <h1 class="display-3"><?php echo $procedures ?></h1>
    <h1 class="display-5">Please choose a form.</h1>
    <p class="lead"><em>The database returned multiple forms with that information.</em></p>
    <form class="" action="createStudent.php" method="post">

      <div class="container p-2 rounded shadow">
        <div class="row ">
          <div class="col-sm-10 offset-1">
            <table class="table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Date Completed</th>
                  <th>P/F</th>
                </tr>
              </thead>
              <tbody>
		<?php
		  $getUserID = mysqli_query($conn, "SELECT StudentID FROM Student WHERE CONCAT(FirstName, ' ', LastName) = '".$user."'");
		  $userIDRow = mysqli_fetch_array($getUserID);
		  $UserID = $userIDRow['StudentID'];
		  $getProcedureID = mysqli_query($conn, "SELECT ProcedureID FROM Proc WHERE ProcedureName = '".$procedures."'");
		  $procedureRow = mysqli_fetch_array($getProcedureID);
		  $ProcID = $procedureRow['ProcedureID'];
		  $numOfForms = mysqli_query($conn, "SELECT COUNT(FormID) AS 'Forms' FROM GreenForm WHERE GreenForm.StudID = '".$UserID."' AND ProcID = '".$ProcID."'");
		  $passOrFail = mysqli_query($conn, "SELECT Pass FROM GreenForm WHERE StudID = '".$UserID."' AND ProcID = '".$ProcID."'");
		  while($r = mysqli_fetch_array($numOfForms)) {
			$number = $r['Forms'];
		  }
		  while($passing = mysqli_fetch_array($passOrFail)) {
			$pass = $passing['Pass'];
		  }
		if($pass == 1) {
			$p = 'P';
		} else {
			$p = 'F';
		} 
		for($i = 1; $i <= $number; $i++) {	
                  echo '<tr>';
                  echo '<th scope="row">'.$i.'</th>';
                  echo '<td>10/02/2018</td>';
                  echo '<td>'.$p.'</td>';
                  echo '</tr>';
		}
		?>
              </tbody>
            </table>
          </div>
        </div>

      </div>
      <div class="row">

      </div>
    </form>
  </div>
</div>




  </body>
</html>




<?php
# mysql login info
#require('dbconn.php');

#getting the username that was filled out
#username=$_POST['username'];

#getting the password that was entered and verifing it.
#$pwd = password_hash($_POST['password'], PASSWORD_DEFAULT);

# sql command to check password
#$sql = "INSERT INTO $tab_name(UserID, Password, Admin, Clinician) VALUES('$username', '$pwd', 1, 0)";
#$sql = "UPDATE Student SET Password = '$pwd', Email = 'bremur5028@students.ecpi.edu' WHERE StudentID = 1";

#checking if query can run or not
// if(!mysqli_query($conn, $sql)) {
	// echo ("Error: " . mysqli_error($conn));
//}
// echo "<br>";
// echo "Hey, " . $username;
// echo "<br>";





#closes connection
// mysqli_close($conn);




# DEBUG FOR ERRORS
#ini_set('display_errors', 1);




?>
