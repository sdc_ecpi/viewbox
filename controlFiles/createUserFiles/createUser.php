<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.css" />

    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-success justify-content-center py-0">

      <a class="navbar-brand"><em>Creating New User</em></a> <!-- make this text light -->

    </nav>

    <div class="jumbotron jumbotron-fluid shadow">
  <div class="container">
    <h1 class="text-center">What type of user?</h1>
    <form class="" action="createStudent.php" method="post" id="form">

      <div class="container p-2 rounded shadow">
        <div class="row ">
          <div class="col-sm-4 offset-2">
            <select class="custom-select" name="usertype">
              <option name="student" value="student" onclick="createUserHandler(this.id)" id="stud"selected>Student</option>
              <option name="clinician" value="clinician" onclick="createUserHandler(this.id)" id="clin">Clinician</option>
              <option name="admin" value="admin" onclick="createUserHandler(this.id)" id="admin">Admin</option>
            </select>
          </div>
          <div class="col-sm-6 ">
            <button type="submit" class="btn btn-primary btn-block">Continue</button>
            <!-- add action to go to new page -->
          </div>
        </div>
        <script type="text/javascript">
        //this is the router for each button. this sets the path for each button for the iframe to go to.
          const pageMap = {
            "stud": "createStudent.php",  //initial loading path
            "clin": "createClinician.php",
            "admin": "createAdmin.php",


          }
          let form = document.getElementById('form');
          let createUserHandler = ((button) => {
            console.log("redirecting");
            form.action = pageMap[button];
          });
        </script>
      </div>
      <div class="row">
        <div class="col-sm-8 offset-2">

        <dl class="dl-horizontal mt-2">
          <dt class="col-sm-3">Students</dt>
          <dd class="col-sm-9">Students will only be able to view their own completed evaluations and view overall progress</dd>
          <dt class="col-sm-3">Clinicians</dt>
          <dd class="col-sm-9">Clinicians can submit new forms and modify existing forms on a given student.</dd>
          <dt class="col-sm-3">Administrators</dt>
          <dd class="col-sm-9">Admins will have full priveledges to create, modify, or delete any account type.</dd>
        </dl>
      </div>
      </div>
    </form>
  </div>
</div>




  </body>
</html>




<?php
# mysql login info
#require('dbconn.php');

#getting the username that was filled out
#username=$_POST['username'];

#getting the password that was entered and verifing it.
#$pwd = password_hash($_POST['password'], PASSWORD_DEFAULT);

# sql command to check password
#$sql = "INSERT INTO $tab_name(UserID, Password, Admin, Clinician) VALUES('$username', '$pwd', 1, 0)";
#$sql = "UPDATE Student SET Password = '$pwd', Email = 'bremur5028@students.ecpi.edu' WHERE StudentID = 1";

#checking if query can run or not
// if(!mysqli_query($conn, $sql)) {
	// echo ("Error: " . mysqli_error($conn));
//}
// echo "<br>";
// echo "Hey, " . $username;
// echo "<br>";





#closes connection
// mysqli_close($conn);




# DEBUG FOR ERRORS
#ini_set('display_errors', 1);




?>
