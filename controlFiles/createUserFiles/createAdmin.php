<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.css" />
<!-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>    <meta charset="utf-8"> -->
    <title></title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-success justify-content-center py-0">

      <a class="navbar-brand" href="#"><em>Creating New Administrator</em></a>

    </nav>

    <div class="jumbotron jumbotron-fluid shadow">
      <div class="container" id="app">
        <h1 class="display-4 text-center">Administrator Info:</h1>
      <form class="" action="createdUser.php" method="post">
        <div class="container p-3 rounded shadow">
          <!-- begin admin name row -->
          <!-- <div class="row">
            <div class="col-sm-5">
              <div class="input-group input-group-lg">
                <div class="input-group-prepend">
                  <span class="input-group-text">First Name</span>
    	          </div>
               <input type="text" name="fName" value="" class="form-control" maxlength="50">
              </div>
            </div>
            <div class="col-sm-2">
              <div class="input-group input-group-lg">
                <div class="input-group-prepend">
                  <span class="input-group-text">M.I.</span>
    	          </div>
               <input type="text" name="MI" value="" class="form-control" maxlength="5">
              </div>
            </div>
            <div class="col-sm-5">
              <div class="input-group input-group-lg">
                <div class="input-group-prepend">
                  <span class="input-group-text">Last Name</span>
    	         </div>
               <input type="text" name="lName" value="" class="form-control" maxlength="255">
              </div>
            </div>
          </div> -->
           <!-- end student name row -->
          <br>
            <div class="row"> <!-- begin ecpi id row -->

              <div class="col-sm-6">
                <div class="input-group input-group-lg">
                  <div class="input-group-prepend">
                    <span class="input-group-text">ECPI ID</span>
      	          </div>
                 <input type="text" name="ecpiID" value="" class="form-control" v-bind.key="studId" maxlength="10">

                </div>
              </div>
              <div class="col-sm-6">
                <div class="input-group input-group-lg">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Email</span>
      	          </div>
                 <input type="text" name="email" v-model="studId" class="form-control" placeholder="" maxlength="255">
                </div>
              </div>

            </div> <!-- end ID and email row -->
            <br>
            <!-- begin date row -->
            <!-- <div class="row">

              <div class="col-sm-4">
                <div class="input-group input-group-lg">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Begin Date</span>
      	          </div>
                 <input type="date" name="beginDate" class="form-control" placeholder="" min="1990-01-01">
                </div>
              </div>
              <div class="col-sm-4">
                <hr>
              </div>
              <div class="col-sm-4">
                <div class="input-group input-group-lg">
                  <div class="input-group-prepend">

                    <span class="input-group-text">Position</span>
      	          </div>
                 <input type="text" name="position" class="form-control" placeholder="" min="1990-01-01">
                </div>
              </div>

            </div>  -->
            <!-- end date row-->
            <br>
            <div class="row">
                <div class="col-sm-2">
                  <button type="button" class="btn btn-secondary" onclick="location.href='http://10.85.145.84/controlFiles/createUserFiles/createUser.php'">Go Back</button>
                </div>
                <div class="col-sm-8">
                  <hr>
                </div>
                <div class="col-sm-2">
                  <button type="submit" class="btn btn-primary">Create Admin</button>
                </div>
            </div>
          </div> <!-- END FORM CONTAINER -->
        </form>
        <div class="row">
          <div class="col-sm-8 offset-2">
            <div class="alert alert-warning" role="alert">
              <strong>Warning!</strong> This person will have power to modify student's records, create new users, etc. 
            </div>

          </div>
        </div>
      </div>
  </div>
</div>


<!-- <script>
  var app = new Vue({
    el: '#app',
    data: {
      emailsuffix: '@students.ecpi.edu',
      studId: ''
    },
    //computed: {
    //   emailAddress:
    // }
  });
</script> -->

  </body>
</html>




<!--
?php
# mysql login info
require('dbconn.php');

#getting the username that was filled out
$FirstName = $_POST['fName'];
$MiddleInitial = $_POST['MI'];
$LastName = $_POST['lName'];
$EcpiID = $_POST['ecpiID'];
$Email = $_POST['email'];
$BeginDate = date("Y-m-d", strtotime($_POST['beginDate']));
$GradDate = date("Y-m-d", strtotime($_POST['gradDate']));
$Cohort = $_POST['cohort'];

$insertStudent = "INSERT INTO Student(FirstName, MiddleInitial, LastName, ECPIID, Email, Password, StartDate, GradDate, Cohort) VALUES('$FirstName', '$MiddleInitial', '$LastName', '$EcpiID', '$Email', 'MCIStudent', '$BeginDate', '$GradDate', $Cohort)";

$checkStud = "SELECT ECPIID FROM STUDENT WHERE ECPIID = '$EcpiID'";

#Checking if the student already exists

if(mysqli_num_rows($checkStud) > 0) {
	echo 'User already exists! Please use the Modify User to change things.';
}
else if(mysqli_num_rows($checkStud) == 0) {
	$insert = mysqli_query($conn, $insertStudent);
	header("Location:http://10.85.145.84/controlFiles/createUserFiles/createdUser.php");
	exit;
}
else {
	echo 'What did you do wrong?';
}

################# ERROR CHECKING #####################
#checking if query can run or not
// if(!mysqli_query($conn, $sql)) {
	// echo ("Error: " . mysqli_error($conn));
//}
// echo "<br>";
// echo "Hey, " . $username;
// echo "<br>";
#####################################################




#closes connection
// mysqli_close($conn);




# DEBUG FOR ERRORS
#ini_set('display_errors', 1);




?>
 -->
