<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
		$(document).ready(function() {
			$("#users").change(function() {
				var user_id = $(this).val();
				if(user_id != "") {
					$.ajax({
						url: "ajaxTest.php",
						data: {u_id:user_id},
						type: 'POST',
						success: function(response) {
								$("#proc").html(response);
					}});
				} else {
					$("#proc").html("<option value=''>---- Select Procedure ----</option>");
				}
			});
		});
	</script>
	</head>
	<body>
		
			<div>
			<select name="users" id="users">
				<option value=''>-------- Select Student --------</option>
				<?php
					require 'dbconn.php';
					$getStudents = mysqli_query($conn, "SELECT CONCAT(FirstName, ' ', LastName) AS 'Name' FROM Student");

					while($getStudentsRow = mysqli_fetch_array($getStudents)) {
						echo "<option value='".$getStudentsRow['Name']."'>".$getStudentsRow['Name']."</option>";
					}
				?>
			</select>
			</div>
			<div>
			<select name="procedure" id="proc">
				<option value=''>---- Select Procedure ----</option>
			</select>
			</div>
		
<?php

                 if(isset($_POST['u_id'])) {
                         $getStudID = mysqli_query($conn, "SELECT StudentID FROM Student WHERE CONCAT(FirstName, ' ', LastName) = '".$_POST['u_id']."'");
                         $getStudIDRow = mysqli_fetch_array($getStudID);
                         $StudID = $getStudIDRow['StudentID'];
                         $res = mysqli_query($conn, "SELECT ProcedureName FROM Proc WHERE ProcedureID IN (SELECT ProcID FROM GreenForm WHERE StudID = '$StudID')");
                         if(mysqli_num_rows($res) > 0) {
                                 while($row = mysqli_fetch_array($res)) {
                                         echo '<option value="'.$row['ProcedureName'].'">'.$row['ProcedureName'].'</option>';
                                 }   
                         } else {
                                 echo '<option value="">No Record</option>';
                         }   
                 }   
         ?>  

	</body>
</html> 
