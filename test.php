<html>
	<head>
		<title>Test</title>
	</head>
	<body>
	<form method="post" action="<?php $_SERVER['PHP_SELF']?>">
			<span>Procedure</span>
			<select name="procs" id="drop" style="width:150px">
			<?php
				require 'dbconn.php';
				$conn = mysqli_connect($servername, $username, $password, $db_name);

				$ProcNames=mysqli_query($conn, "select ProcedureName from Proc");
				while($row=mysqli_fetch_array($ProcNames))
				{
					echo "<option value='".$row['ProcedureName']."'>".$row['ProcedureName']."</option>";
				}
				
			?>
			</select>
			<br />
			<span>Student</span>
			<select name="student" style="width:150px">
			<?php
				$StudNames=mysqli_query($conn, "SELECT CONCAT(FirstName, ' ', LastName) AS Name FROM Student");
				while($studRow=mysqli_fetch_array($StudNames))
				{
					echo "<option value='".$studRow[0]."'>".$studRow[0]."</option>";
				}
			?>
			</select>
			<br />
			<span>Site</span>
			<select name="site" style="width:150px">
			<?php
				$SiteNames=mysqli_query($conn, "SELECT CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) AS Site FROM ClinicalSite");
				while($siteRow=mysqli_fetch_array($SiteNames))
				{
					echo "<option value='".$siteRow[0]."'>".$siteRow[0]."</option>";
				}
			?>
			</select>
			<br />
			<span>Comp Date</span>
			<input type="date" name="compDate">
			<?php
				$compDate = date("Y-m-d", strtotime($_POST['compDate']));
			?>
			<br />
			<span>Observation Date</span>
			<input type="date" name="obsDate">
			<?php
				$obsDate = date("Y-m-d", strtotime($_POST['obsDate']));
			?>
			<br />
			<span>Observation Site</span>
			<select name="obsSite" style="width:150px">
			<?php
				$ObsSiteNames=mysqli_query($conn, "SELECT CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) AS SITE FROM ClinicalSite");
				while($obsSiteRow=mysqli_fetch_array($ObsSiteNames))
				{
					echo "<option value='".$obsSiteRow[0]."'>".$obsSiteRow[0]."</option>";
				}
			?>
			</select>
			<br />
			<span>Assist Date</span>
			<input type="date" name="assDate">
			<?php
				$assDate = date("Y-m-d", strtotime($_POST['assDate']));
			?>
			<br />
			<span>Assist Site</span>
			<select name="assSite" style="width:150px">
			<?php
				$AssSiteNames=mysqli_query($conn, "SELECT CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) AS Site FROM ClinicalSite");
				while($assSiteRow=mysqli_fetch_array($AssSiteNames))
				{
					echo "<option value='".$assSiteRow[0]."'>".$assSiteRow[0]."</option>";
				}
			?>
			</select>
			<br />
			<span>Evalualtion Yes/No  </span>
			<?php
				$i=0;
				$j=0;
				$EvalTotal=0;
				$getEvalInfo=mysqli_query($conn, "SELECT `1`,`2`,`3`,`4`,`5`,`6`,`7`,`8`,`9`,`10`,`11`,`12`,`13`,`14`,`15`,`16`,`17`,`18`,`19`,`20`,`21`,`22`,`23`,`24`,`25` FROM Eval");
				while($EvalInfo=mysqli_fetch_array($getEvalInfo))
				{
					for($count = 0; $count < 25; $count++)
					{
						if($EvalInfo[$count] == 4)
						{
							$i = $i + 1;
						}
						else
						{
							$j = $j + 1;
						}
						$EvalTotal += $EvalInfo[$count];
					}
				}
				echo $i.'/'.$j;
			?>
			<br />
			<span>Evaluation Score   </span>
			<?php
			#	echo $EvalTotal;
?>

			<button type="submit">Submit</button>
			</form>
			<?php
				$proc = $_POST['procs'];
				$stud = $_POST['student'];
				$site = $_POST['site'];
				//$obsDate = $_POST['obsDate'];
				$obsSite = $_POST['obsSite'];
				//$assDate = $_POST['assDate'];
				$assSite = $_POST['assSite'];

				$getProcID=mysqli_query($conn, "SELECT ProcedureID FROM Proc WHERE ProcedureName = '$proc'");
				$row2 = mysqli_fetch_array($getProcID);
				$ProcID = $row2['ProcedureID'];
				echo $ProcID;

				$getStudID=mysqli_query($conn, "SELECT StudentID FROM Student WHERE CONCAT(FirstName, ' ', LastName) = '$stud'");
				$studRow2 = mysqli_fetch_array($getStudID);
				$StudID = $studRow2['StudentID'];
				echo $StudID;
				
				$getSiteID = mysqli_query($conn, "SELECT SiteID FROM ClinicalSite WHERE CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) = '$site'");
				$siteRow2 = mysqli_fetch_array($getSiteID);
				$SiteID = $siteRow2['SiteID'];
				echo $SiteID;

				$getObsSiteID = mysqli_query($conn, "SELECT SiteID FROM ClinicalSite WHERE CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) = '$obsSite'");
				$obsSiteRow2 = mysqli_fetch_array($getObsSiteID);
				$obsSiteID = $obsSiteRow2['SiteID'];
				echo $obsSiteID;

				$getAssSiteID = mysqli_query($conn, "SELECT SiteID FROM ClinicalSite WHERE CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) = '$assSite'");
				$assSiteRow2 = mysqli_fetch_array($getAssSiteID);
				$assSiteID = $assSiteRow2['SiteID'];
				echo $assSiteID;

				echo "<br /><br /><br />";

				
				$i=0;
				$j=0;
				$EvalTotal=0;
				$getEvalInfo=mysqli_query($conn, "SELECT `1`,`2`,`3`,`4`,`5`,`6`,`7`,`8`,`9`,`10`,`11`,`12`,`13`,`14`,`15`,`16`,`17`,`18`,`19`,`20`,`21`,`22`,`23`,`24`,`25` FROM Eval JOIN GreenForm ON Eval.EvalID = GreenForm.EvalID WHERE GreenForm.ProcID = '$ProcID'");
				while($EvalInfo=mysqli_fetch_array($getEvalInfo))
				{
					for($count = 0; $count < 25; $count++)
					{
						echo "<select> <option>".$EvalInfo[$count]."</option> </select>";
						if($EvalInfo[$count] == 4)
						{
							$i = $i + 1;
						}
						else
						{
							$j = $j + 1;
						}
						$EvalTotal += $EvalInfo[$count];
					}
				}
				#echo $i.'/'.$j;
				echo $EvalTotal;


#				$greenFormInsert = "INSERT INTO GreenForm(StudID, ProcID, CompDate, SiteID, ObservationDate, ObservationSig, ObservationSiteID, AssistDate, AssistSig, AssistSiteID, EvalID, StudentSig, Comments, ProficiencyExam) VALUES ('$StudID', '$ProcID', '$compDate', '$SiteID', '$obsDate', 1, '$obsSiteID', '$assDate', 1, '$assSiteID', 1, 1, 'Blah', 0)";
#				if(!mysqli_query($conn, $greenFormInsert)) {
#					echo ("Error: ".mysqli_error($conn));
				#				}

   			?>
		</body>
</html>
