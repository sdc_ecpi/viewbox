<!-- TODO:
  fix dropdown on procedure field
  fix submit button alignment
  fix functionality of yes/no in evals
  fix dimensions of print and signature
  add proficiency
 -->

 <?php
  session_start();

  #if(!isset($_SESSION['username'])) {
   # header('Location: http://10.85.145.84');
   # die();
  #}


 ?>



 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title>Green Form Template</title>
     <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/signature_pad/1.5.3/signature_pad.js" integrity="sha256-4fSHT/rSaTeezfEowRbkPD1GAz8f/k9SO/eh7ZTOBEg=" crossorigin="anonymous"></script>


     <style media="screen">
       .table-borderless td, .table-borderless th {
         border: none;
       }

       .obsWrapper {
	   position: relative;
	   width: 798px;
	   height: 185px;
       }

       .signaturePad-OA {
	   position: absolute;
	   left: 0;
	   right: 0;
           width: 798px;
	   height: 168px;
	   background-color: white;
       }

       .active {
         background-color: blue !important;
         /* change this bitch */
       }

       input:checked {
         background-color: yellow;
       }
     </style>





   </head>
   <body>
	<?php
  		require '../../dbconn.php';
		$StudAndProc="StudID = (SELECT StudentID FROM Student WHERE CONCAT(FirstName, ' ', LastName) = '".$_SESSION['StudentName']."') AND ProcID = (SELECT ProcedureID FROM Proc WHERE ProcedureName = '".$_SESSION['ProcName']."')";
	?>
     <!-- style="background-color: #d7eddd"    #61b579-->
     <!-- nav bar with editing commands and buttons -->
    <div class="container container-fluid mt-2 mb-5 shadow-lg" style="background-color: #5ec177"> <!--begin transcribe from green form-->
    <form id="greenForm" class="pt-2" action="greenFormCreation.php" method="post" >
     <div class="form-group">


       <div class="row" id="Univ-header">

         <div class="col-sm-10 offset-sm-1">
           <p class="text-center" style="font-weight: bolder;">MEDICAL CAREERS INSTITUTE COLLEGE OF HEALTH SCIENCE OF ECPI UNIVERSITY MEDICAL RADIOLOGY PROGRAM</p>
           <br>
           <h1 class="lead text-center">CLINICAL COMPETENCY EVALUATION FORM</h1> <!-- BEGIN FIRST SUBHEADING -->

         </div>
         <div class="col-sm-2">

         </div>
         <h4 ></h4>

     </div> <!-- END UNIV HEADER -->
           <br>
      <div class="row" id="form-meta-info"> <!-- FORM META INFO ROW 1 -->
        <div class="col-sm-6 ">
          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">Student Name</span>
	    </div>
	      <input type="text" class="form-control" name='stud' value="<?php
				$StudentName = $_SESSION['StudentName'];
				echo $StudentName;
				?>" disabled />

          </div>
        </div>
        <div class="col-sm-6">
          <!-- QUERY DATABASE FOR PROCEDURES -->
          <div class="input-group input-group-sm ">
            <div class="input-group-prepend">
              <span class="input-group-text">Procedure</span>
            </div>

	      <input type="text" class="form-control" name='proc' value='<?php
				$ProcedureName = $_SESSION['ProcName'];
				echo $ProcedureName;
				?>' disabled />

            <!-- M/E/S? -->
          </div>
        </div>
      </div> <!-- END FORM META INFO ROW 1-->
        <br>
        <div class="row">  <!-- BEGIN FORM META INFO ROW 2 -->
          <div class="col-sm-7">
            <div class="input-group input-group-sm">
              <div class="input-group-prepend">
                <span class="input-group-text">Competency Date</span>
	      </div>

              <input type="date" name="compDate" class="form-control">
			<?php
#				$compDate = date("Y-m-d", strtotime($_POST['compDate']));
			?>

            </div>
          </div>

          <div class="col-sm-5">

              <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                  <span class="input-group-text">Clinical Site</span>
                </div>

		<select name="site" class="custom-select custom-select-sm">
			<?php
				$SiteNames=mysqli_query($conn, "SELECT CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) AS Site FROM ClinicalSite");
				while($siteRow=mysqli_fetch_array($SiteNames))
				{
					echo "<option value='".$siteRow[0]."'>".$siteRow[0]."</option>";
				}
			?>
		</select>

            </div>
          </div>

        </div> <!-- END FORM META INFO ROW 2 -->
        <br>
        <h2 class="lead text-center">DOCUMENTATION by R.T (R)</h2> <!-- BEGIN SECOND HEADING -->
        <div class="row"> <!-- BEGIN DOCUMENTATION TABLE -->


          <table class="table table-borderless ">


              <tr class="shadow-sm text-center " style="background-color: #00ad6a;">
                <th></th>
                <th>Date</th>
                <th>Print</th>
                <th>Signature</th>
                <th>Site</th>
              </tr>



              <tr>
                <th scope="row">1: Observation</th>
                <td><input type="text" name="obsDate" class="form-control form-control-sm" value="Working on Eval" disabled></td>
                <td><input type="text" name="obsPrint" class="form-control form-control-sm" value="Working on Eval" disabled></td>
                <td><input type="text" name="obsSig" class="form-control form-control-sm" value="Working on Eval" disabled></td>
                <td><input type="text" name="obsSite" class="form-control form-control-sm" value="Working on Eval" disabled></td>
              </tr>
              <tr>
                <th scope="row">2: Assisted</th>
                <td><input type="text" name="assDate" class="form-control form-control-sm" value="Working on Eval" disabled></td>
                <td><input type="text" name="assPrint" class="form-control form-control-sm" value="Working on Eval" disabled></td>
                <td><input type="text" name="assSig" class="form-control form-control-sm" value="Working on Eval" disabled></td>
                <td><input type="text" name="assSite" class="form-control form-control-sm" value="Working on Eval" disabled></td>
              </tr>

          </table>


        </div> <!-- END DOCUMENTATION TABLE -->
        <hr>

        <!-- STUDENT COMMENT IF REQUIRED-->
        <div class="row"> <!-- EVALUATION CRITERIA HEADING-->
        <div class="col-sm-4 offset-4">
          <h2 class="lead text-center">EVALUATION CRITERIA</h2>

        </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="text-center">

            <p><strong>Competency Requirement:</strong> Perform the Radiologic procedure  appropriately, utilizing these criteria:</p>
            <p><strong>There are 25 category criteria ("Yes" / "No")</strong></p>
            <p>All projections for a given examination must be performed correctly in order to recieve a "Yes" for the category.</p>
          </div>
          </div>
        </div>
      </div> <!-- END FORM GROUP -->
      <div class="row">

      <table class="table table-sm table-borderless table-hover"> <!-- BEGIN EVALUATION TABLE -->
        <tbody>

            <!--
            This section screams out to be generated in php. All of the below will be populated by the database, as these 25 criteria can change depending on the type of evaluation, etc.
            php will be rendering pretty much all of this, but here's the template to use. I'll adjust the sizing and spacing, don't worry about that.
           -->

           <?php

           $questionArray = array(
             "1"=>"Identifies patient using two patient identifiers",
             "2"=>"Verifies requisition is applicable according to anatomical part and history",
             "3"=>"Obtains patient's history; inquires about possible pregnancy (if applicable)",
             "4"=>"Cleans room/table/wall unit; linens, pillow, table pad etc.",
             "5"=>"Equipment readiness (Table, tube, girds, etc)",
             "6"=>"Accesories ready (i.e, Flouroscopy aprons, contrast media, etc.)",
             "7"=>"Communicates & assess patient throughout the exam (Respiration, appearance, etc.)",
             "8"=>"Provides patient assistance & comfort throughout the exam (transfer, positioning, etc)",
             "9"=>"Makes adjustments for medical Equipment: I.V / 02 / tubing/leads, etc. (if applicable)",
             "10"=>"Correct tube and Table Handling (SID, Locks, Detent, Movements & Angles)",
             "11"=>"Correct selection of Technical Factors: ",
             "12"=>"Correct Receptor Selection (Table top/bucky/IR alignment/ Size / Placement)",
             "13"=>"Performs the correct Projection/Position(s) for Anatomical Part (AP/PA/Lateral, etc.)",
             "14"=>"Correct Body Part alignment (Erect/Supine/Prone/Oblique/Lateral)",
             "15"=>"Correct central ray placement",
             "16"=>"Correct use of Anatomical Markers and Positioning Aids",
             "17"=>"Provides proper Protection (Sheilding / Collimation / Technique)",
             "18"=>"Ensure Self and Others are Protected (sheilding / barrier / Distance / Exposure Announcement)",
             "19"=>"Correctly inputs & verifies patient demographic information on workstation/ PACS",
             "20"=>"Correctly exposes and processes the image (processor/computer)",
             "21"=>"Images demonstrate accurate positioning & demonstrate all required anatomy",
             "22"=>"Images demonstrate acceptable density, contrast and collination",
             "23"=>"Images are free of artifacts, distortion, and motion, etc.",
             "24"=>"Completes exam in a timely manner & patient given follow-up instructions",
             "25"=>"Correctly identifies at least three(3) anatomical structures <em>(no partial credit allowed)</em>",
           );
           for($iter = 1; $iter < 26; $iter++) {
             if($iter == 1) {
               echo '
                <tr scope="row" style="background-color: #00ad6a;" class="">
                  <th scope="row" class="">&nbspA. <span class="text-center" >Evaluation of Procedure Requisistion</span></th>
                  <td></td>';
             } else if($iter == 4) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" >
                  <th scope="row">&nbspB. Room Preparation</th>
                  <td></td>';
             } else if($iter == 7) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" >
                 <th scope="row" class="">&nbspC. Patient Assesment and Management</th>
                 <td></td>';
             } else if($iter == 10) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row" class="">&nbspD. Equipment Operation and Technique</th>
                 <td></td>';
             } else if($iter == 13) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspE. Positioning Skills</th>
                <td></td>';
             } else if($iter == 17) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspF. Radiation Safety</th>
                 <td></td>';
             } else if($iter == 19) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspG. Image Aquisition</th>
                 <td></td>';
             } else if($iter == 21) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspH. Image analysis and Examination Completion</th>
                 <td></td>';
             } else if($iter == 25) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspI. Anatomical Parts Identification</th>
                 <td></td>';
             }
           echo '
           <tr>
            <td scope="row" class="pl-4 pt-3">' . $iter . ' <span class="">' . $questionArray[$iter] . '</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary " id="yesbtn' . $iter . '" onclick="highlight(this.id)">
	      <input type="radio" name="options' . $iter . '" value="yes" autocomplete="off" required><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary" id="nobtn' . $iter . '" onclick="highlight(this.id)">
	      <input type="radio" name="options' . $iter . '" value="no" autocomplete="off" required><span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>';}



          ?>
<script type="text/javascript">
  // var highlight = (id) => {
  //
  //   document.getElementById(id).style="background-color: black;"
  //
  //   console.log(`id: ${id}`);
  // }
  //
  // if(document.getElementById(id).style="background-color: black;" && )


</script>

         </tr>
        </tbody>
      </table>
    </div> <!-- END EVALUATION SECTION -->
      <hr>
      <div class="row">
        <div class="col-sm-10 offset-1">

          <p class="text-center">Note: R.T (R) may have the student identify specific anatomy or ask the student to point to requested anatomy</p>
        </div>

      </div>
      <div class="row">
        <div class="col-sm-2">
          <div>
            <p><strong>List Anatomy:</strong></p>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">(1)</span>
            </div>
            <input type="text" name="bodypart1" value="" class="form-control">
          </div>

        </div>
        <div class="col-sm-3">
          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">(2)</span>
            </div>
            <input type="text" name="bodypart2" value="" class="form-control">
          </div>
        </div>
        <div class="col-sm-3">


          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">(3)</span>
            </div>
            <input type="text" name="bodypart3" value="" class="form-control">
          </div>
        </div>
      </div>
      <br>
<!-- FIX THE RESPONSIVENESS ISSUE BITCH -->
      <div class="row">
        <div class="col-sm-2">
          <div>
            <p><strong>Comments:</strong></p>
          </div>
        </div>
        <div class="col-sm-8 ">
          <div class="input-group ">
            <textarea name="evalComments" rows="2" cols="60" maxlength="255" class="align-center form-control"></textarea>
          </div>
        </div>

      </div>
      <hr>
      <div class="row"> <!-- COMPETENCY EVALUATION HEADING-->
      <div class="col-sm-4 offset-4">
        <h2 class="lead text-center">COMPETENCY EVALUTION</h2>

      </div>
      </div>


      <div class="row"> <!-- COMPETENCY POLICY NOTES -->
        <div class="col-sm-10 offset-1">
          <div class="text-center">

          <p><strong>A minimum of 73 points (73% = "C" minimum passing) is required for a completion of a competency.
            <br>ARRT requires that failing scores must be recorded and the exam to be repeated for documenting competency.</strong></p>
          </div>
        </div>
      </div>
<hr>
      <div class="row">
        <div class="col-sm-8 offset-1 pt-3">
          <p><strong>Do you feel this student can perform this examination consistently and competently?</strong></p>
        </div>
        <div class="col-sm-2 offset-1">
          <div class="btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
          <label class="btn btn-secondary">
      <input type="radio" name="consistent" value="yes" autocomplete="off"> <span style="font-weight: bold;">Yes</span>
          </label>
          <label class="btn btn-secondary">
      <input type="radio" name="consistent" value="no" autocomplete="off"> <span style="font-weight: bold;">No</span>
          </label>
          </td>
          </div>
        </div>
      </div>

<hr>


      <div class="row">
	<div class="col-sm-8 offset-1 pt-3">
	   <p><strong>Is this a proficiency exam?</strong></p>
	</div>
	<div class="col-sm-2 offset-1">
  <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
    <label class="btn btn-secondary">
	<input type="radio" name="proficiency" value="yes" autocomplete="off"><span style="font-weight: bold;">Yes</span>
	  </label>
	  <label class="btn btn-secondary">
	<input type="radio" name="proficiency" value="no" autocomplete="off"><span style="font-weight: bold;">No</span>
	  </label>
	</div>
      </div>
    </div>

<hr>

<div class="row"> <!-- COMPETENCY EVALUATION HEADING-->
  <div class="col-sm-4 offset-4">
    <h2 class="lead text-center">Any Further Comments:</h2>
  </div>
</div>
<br>
  <div class="row">
    <div class="col-sm-8 offset-2">
      <div class="input-group ">
        <textarea name="comments" rows="4" cols="82" class="form-control" maxlength="255"></textarea>
      </div>
    </div>
  </div>

  <hr>

 <!-- <div class="row">
  <div class="col-sm-10 offset-1">
    <div class="input-group input-group-sm">
      <div class="input-group-prepend">
        <span class="input-group-text">Clinical Instructor R.T</span>
      </div>
      <input type="text" name="clinicalPrint" value="" class="form-control">
    </div>
  </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-6 offset-3">
      <p class="lead">Clinical Signature</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2 offset-1">
      <p class="lead mt-2">Please sign here:<p/>
    </div>
    <div class="col-sm-6">
    <div id="clinSig">
      <canvas width="350" height="80" style = "background-color: white"></canvas>
    </div>
    </div>
    <div class="col-sm-1">
      <button type="button" class="btn btn-secondary" onclick="clinSigPad.clear()">Clear</button>
    </div>
    <script>
    //signaturepad documentation: https://github.com/szimek/signature_pad
    </script>
  </div>
<br>
<hr>-->
<br>

<div class="row">
  <div class="col-sm-6 offset-3">
    <p class="lead">Student Signature</p>
  </div>
</div>
<div class="row">
  <div class="col-sm-2 offset-1">
    <p class="lead mt-2">Please sign here:<p/>
  </div>
  <div class="col-sm-6">
  <div id="studSig">
    <canvas width="350" height="80" style = "background-color: white"></canvas>
  </div>
  </div>
  <div class="col-sm-1">
    <button type="button" class="btn btn-secondary" onclick="signaturePadStud.clear()">Clear</button>
  </div>
  <script>
  //signaturepad documentation: https://github.com/szimek/signature_pad
  </script>
</div>
<br>
<br>
    <div class="row pb-4">
      <div class="col-sm-6 offset-2">
        <button type="button" class="btn btn-danger" name="goback" onclick="href.location='http://10.85.145.84/clinicianPage.php'">Go Back</button>
      </div>

      <div class="col-sm-2 offset-1">
        <button type="button" class="btn btn-primary" id="submitBtn" class="float-right">Submit</button>
      </div>
    </div>
<input type="hidden" id="studDataInfo" name="studData" value="" />
<input type="hidden" id="stud" name="stud" value="" />
    </form>

  <script>
/*	var wrapper1 = document.getElementById("clinSig"),
	    clinCanvas = wrapper1.querySelector('canvas'),
	    clinSigPad;
*/
	var studCanvas = document.querySelector('canvas');

//	var clinSigPad = new SignaturePad(clinCanvas);
	var signaturePadStud = new SignaturePad(studCanvas);

/*	function resizeCanvas() {
		var ratio = Math.max(window.devicePixelRatio || 1, 1);
		obsCanvas.width = obsCanvas.offsetWidth * ratio;
		obsCanvas.height = obsCanvas.offsetHeight * ratio;
		obsCanvas.getContext("2d").scale(ratio, ratio);

	}

	resizeCanvas();
*/

	document.getElementById('submitBtn').addEventListener('click', function() {
		if(signaturePadStud.isEmpty()) {
			return alert("Please provide a signature");
		}
		var data = signaturePadStud.toDataURL('image/png');
		document.getElementById("studDataInfo").value = data;
		document.getElementById("stud").value = "stud";
		document.forms['greenForm'].submit();
	});
  </script>

  </div> <!-- END CONTAINER -->
  <?php
/*    $stud = $_SESSION['StudentName'];
    $proc = $_SESSION['ProcName'];
    $site = $_POST['site'];
    $evalAnatomy1 = $_POST['bodypart1'];
    $evalAnatomy2 = $_POST['bodypart2'];
    $evalAnatomy3 = $_POST['bodypart3'];
    $evalComments = $_POST['evalComments'];
    $consistent = $_POST['consistent'];
    $proficient = $_POST['proficiency'];
    $comments = $_POST['comments'];

    $getProcID=mysqli_query($conn, "SELECT ProcedureID FROM Proc WHERE ProcedureName = '$proc'");
    $row2 = mysqli_fetch_array($getProcID);
    $ProcID = $row2['ProcedureID'];

    $getStudID=mysqli_query($conn, "SELECT StudentID FROM Student WHERE CONCAT(FirstName, ' ', LastName) = '$stud'");
    $studRow2 = mysqli_fetch_array($getStudID);
    $StudID = $studRow2['StudentID'];

    $getSiteID = mysqli_query($conn, "SELECT SiteID FROM ClinicalSite WHERE CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) = '$site'");
    $siteRow2 = mysqli_fetch_array($getSiteID);
    $SiteID = $siteRow2['SiteID'];

	$dataUrl = $_POST['studData'];
	$rawFileName = $stud.$proc."StudentSig.png";
	$fileName = str_replace(' ', '', $rawFileName);
	$encodedImage = explode(",", $dataUrl, 1);
	$decodedImage = base64_decode($encodedImage);
	file_put_contents("../../signatures".$fileName, $decodedImage);

    if($consistent == 'yes') {
	$consistent = 1;
    }
    else {
	$consistent = 0;
    }

    if($proficient == 'yes') {
	$proficient = 1;
    }
    else {
	$proficient = 0;
    }


    for($count = 1; $count < 26; $count++)
    {
	    if($count == 25) {
		    $EvalCount .= '`'.$count.'`';
	    }
	    else {
		    $EvalCount .= '`'.$count.'`, ';
	    }
    }

	    $exist = mysqli_query($conn, "SELECT * FROM GreenForm WHERE StudID = '$StudID' AND ProcID = '$ProcID'");
echo $ProcID;
	    if(mysqli_num_rows($exist) > 0)
	    {
		    $EvalTotalUpdate = 0;
    		    for($count = 1; $count < 26; $count++) {
	    		$curPost = 'options'.$count;
	    		$EvalUpdateBtn=$_POST[$curPost];

	    		if(isset($EvalUpdateBtn))
	    		{
		    		if($count == 25)
		    		{
		    			if($EvalUpdateBtn == 'yes')
		    			{
						$EvalUpdateEntry .= '`'.$count.'` = '. 4;
						$EvalTotalUpdate += 4;
		    			}
		    			else if($EvalUpdateBtn == 'no')
		    			{
						$EvalUpdateEntry .= '`'. $count .'` = '. 0;
		    			}
		    		}
		    		else
		    		{
					if($EvalUpdateBtn == 'yes')
		      			{
						$EvalUpdateEntry .= '`'.$count.'` = '. 4 . ', ';
						$EvalTotalUpdate += 4;
		    			}
		    			else if($EvalUpdateBtn == 'no')
		    			{
						$EvalUpdateEntry .= '`'.$count.'` = '. 0 . ', ';
					}

		    		}

	    		}
    		    }
############################################################################################################################################################################ UPDATING EVAL TABLE #######################################################################################################################################################################################################################################
		    $EvalIDGFUpd = mysqli_query($conn, "SELECT Eval.EvalID FROM Eval WHERE Eval.EvalID = (SELECT GreenForm.EvalID FROM GreenForm WHERE StudID = '$StudID' AND ProcID = '$ProcID')");
		    $EvalIDGFURow = mysqli_fetch_array($EvalIDGFUpd);
		    $EvalIDGFU = $EvalIDGFURow['EvalID'];
		    $EvalUpd = mysqli_query($conn, "UPDATE Eval SET $EvalUpdateEntry, Anatomy1 = '$evalAnatomy1', Anatomy2 = '$evalAnatomy2', Anatomy3 = '$evalAnatomy3', Comments = '$evalComments' WHERE Eval.EvalID = (SELECT GreenForm.EvalID FROM GreenForm WHERE StudID = '$StudID' AND ProcID = '$ProcID')") or die(mysqli_error($conn));

                    ############################################################################################################################################ CHECKING FOR A PASSING SCORE ####################################################################################################################################################
		    if($EvalTotalUpdate > 73) {
			 $Pass = 1;
		    }
		    else {
		    	 $Pass = 0;
		    }

		    ################################################################################################################################################ UPDATING GREEN FORM ##########################################################################################################################################################
		    $update = mysqli_query($conn, "UPDATE GreenForm SET CompDate = '$compDate', SiteID = '$SiteID', EvalID = '$EvalIDGFU', StudentSig = '$studSigLocation', Consistent = '$consistent', Comments = '$comments', Pass = '$Pass', ProficiencyExam = '$proficient' WHERE StudID = '$StudID' AND ProcID = '$ProcID'");
	    }
	    else
	    {
		$EvalTotal = 0;
    		    for($count=1; $count<26; $count++)
    			{
	    			$curPost = 'options'.$count;
	    			$EvalBtn=$_POST[$curPost];

	    			if(isset($EvalBtn))
	    			{
		    			if($count == 25)
		    			{
		    				if($EvalBtn == 'yes')
		    				{
							$EvalEntry .= 4;
							$EvalTotal += 4;
		    				}
		    				else if($EvalBtn == 'no')
		    				{
							$EvalEntry .= 0;
		    				}
		    			}
		    			else
		    			{
						if($EvalBtn == 'yes')
		      				{
							$EvalEntry .= 4 . ', ';
							$EvalTotal += 4;
		    				}
		    				else if($EvalBtn == 'no')
		    				{
							$EvalEntry .= 0 . ', ';
						}

		    			}

	    			}
    			}
    		    $getEvalID = mysqli_query($conn, "SELECT MAX(EvalID) AS EvalID FROM Eval");
    		    $getEvalIDRow = mysqli_fetch_array($getEvalID);
    		    $MaxEvalID = $getEvalIDRow['EvalID'];
		    ############################################################################################################################################### INSERTING INTO EVAL TABLE #######################################################################################################################################################
		    $EvalIns = mysqli_query($conn,  "INSERT INTO Eval($EvalCount, Anatomy1, Anatomy2, Anatomy3, Comments) VALUES($EvalEntry, '$evalAnatomy1', '$evalAnatomy2', '$evalAnatomy3', '$evalComments')");

    		    if($EvalTotal >= 73) {
		   	 $Pass = 1;
    		    }
    		    else {
			 $Pass = 0;
    		    }
		    $insert = mysqli_query($conn, "INSERT INTO GreenForm(StudID, ProcID, CompDate, SiteID, EvalID, StudentSig, Consistent, Comments, Pass, ProficiencyExam) VALUES ('$StudID', '$ProcID', '$compDate', '$SiteID', '$MaxEvalID', '$studSigLocation', '$consistent', '$comments', '$Pass', '$proficient)");
	    }*/
?>
   </body>
 </html>
