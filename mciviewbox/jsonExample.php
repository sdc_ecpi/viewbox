<?php
	require 'dbconn.php';

	$info = mysqli_query($conn, "SELECT * FROM GreenForm WHERE FormID = 17");
	$sorting = mysqli_fetch_assoc($info);
	$jsonify = json_encode($sorting, JSON_PRETTY_PRINT);

	header('Content-Type: appication/json');
	echo $jsonify;
?>
