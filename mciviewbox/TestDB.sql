-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: Test
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ClinicalSite`
--

DROP TABLE IF EXISTS `ClinicalSite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClinicalSite` (
  `SiteID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteName` varchar(255) NOT NULL,
  `SiteCity` varchar(255) DEFAULT NULL,
  `SiteState` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SiteID`),
  UNIQUE KEY `SiteID_UNIQUE` (`SiteID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ClinicalSite`
--

LOCK TABLES `ClinicalSite` WRITE;
/*!40000 ALTER TABLE `ClinicalSite` DISABLE KEYS */;
INSERT INTO `ClinicalSite` VALUES (1,'Sentara Albemarle Hospital','Elizabeth City','NC'),(2,'Childrens Hospital of Kings Daughters (CHKD)','Norfolk','VA'),(3,'Patient First Cedar Road','Chesapeake','VA'),(4,'Now Care','Virginia Beach','VA'),(5,'SMOC (Sports Medicine & Ortho)','Chesapeake','VA'),(6,'SMOC (Sports Medicine & Ortho)','Suffolk','VA'),(7,'Mary Immaculate Hospital','Newport News','VA'),(8,'Bon Secours Rappahannock General Hospital','Kilmarnock','VA'),(9,'Sentara Williamsburg Region Medical Center','Williamsburg','VA'),(10,'M.D. Express Grafton','Yorktown','VA'),(11,'M.D. Express','Williamsburg','VA'),(12,'M.D. Express','Newport News','VA'),(13,'M.D. Express','Hampton','VA'),(14,'Patient First Denbeigh','Newport News','VA'),(15,'TPMG (Tidewater Physicians Multispecialty Group)','Williamsburg','VA'),(16,'OSC (Orthopaedic and Spine Center)','Newport News (Port Warwick)','VA');
/*!40000 ALTER TABLE `ClinicalSite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Eval`
--

DROP TABLE IF EXISTS `Eval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Eval` (
  `EvalID` int(11) NOT NULL AUTO_INCREMENT,
  `1` int(11) DEFAULT NULL,
  `2` int(11) DEFAULT NULL,
  `3` int(11) DEFAULT NULL,
  `4` int(11) DEFAULT NULL,
  `5` int(11) DEFAULT NULL,
  `6` int(11) DEFAULT NULL,
  `7` int(11) DEFAULT NULL,
  `8` int(11) DEFAULT NULL,
  `9` int(11) DEFAULT NULL,
  `10` int(11) DEFAULT NULL,
  `11` int(11) DEFAULT NULL,
  `12` int(11) DEFAULT NULL,
  `13` int(11) DEFAULT NULL,
  `14` int(11) DEFAULT NULL,
  `15` int(11) DEFAULT NULL,
  `16` int(11) DEFAULT NULL,
  `17` int(11) DEFAULT NULL,
  `18` int(11) DEFAULT NULL,
  `19` int(11) DEFAULT NULL,
  `20` int(11) DEFAULT NULL,
  `21` int(11) DEFAULT NULL,
  `22` int(11) DEFAULT NULL,
  `23` int(11) DEFAULT NULL,
  `24` int(11) DEFAULT NULL,
  `25` int(11) DEFAULT NULL,
  `Anatomy1` varchar(255) DEFAULT NULL,
  `Anatomy2` varchar(255) DEFAULT NULL,
  `Anatomy3` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EvalID`),
  UNIQUE KEY `EvalID_UNIQUE` (`EvalID`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Eval`
--

LOCK TABLES `Eval` WRITE;
/*!40000 ALTER TABLE `Eval` DISABLE KEYS */;
INSERT INTO `Eval` VALUES (1,4,4,4,0,4,4,0,4,4,4,0,4,4,4,4,4,4,4,4,4,4,0,4,4,4,'hey','buddy','hows','it'),(2,4,4,0,4,4,4,4,4,4,4,0,0,0,0,0,4,0,4,4,4,4,4,0,4,4,'Ribs','Clavicle','Shoulder',NULL),(3,4,4,4,0,0,0,4,4,4,0,0,0,4,4,4,0,0,0,4,4,4,0,0,0,4,NULL,NULL,NULL,NULL),(56,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,NULL,NULL,NULL,NULL),(57,4,4,0,4,0,4,4,4,4,4,0,4,4,0,4,0,4,0,4,0,4,4,4,4,4,'b','r','e','n'),(58,4,4,0,4,0,4,4,4,4,4,0,4,4,0,4,0,4,0,4,0,4,4,4,4,4,'b','r','e','n'),(59,4,4,4,0,4,0,4,0,4,4,4,4,0,4,4,NULL,4,0,4,0,4,4,4,0,4,'mr','dr','pro','plum'),(60,4,4,4,0,4,0,4,0,4,4,4,4,0,4,4,NULL,4,0,4,0,4,4,4,0,4,'mr','dr','pro','plum'),(61,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,'e','e','e','e'),(62,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,'e','e','e','e'),(63,4,0,4,4,0,4,0,4,4,4,4,0,4,4,4,4,4,4,4,4,4,4,4,4,4,'jesus','was','here','or'),(64,4,0,4,4,0,4,0,4,4,4,4,0,4,4,4,4,4,4,4,4,4,4,4,4,4,'jesus','was','here','or'),(65,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,4,4,4,4,4,4,4,4,4,4,'checking','on','testie','123');
/*!40000 ALTER TABLE `Eval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GreenForm`
--

DROP TABLE IF EXISTS `GreenForm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GreenForm` (
  `FormID` int(11) NOT NULL AUTO_INCREMENT,
  `StudID` int(11) NOT NULL,
  `ProcID` int(11) NOT NULL,
  `CompDate` date NOT NULL,
  `SiteID` int(11) NOT NULL,
  `ObservationDate` date DEFAULT NULL,
  `ObservationSig` tinyint(4) DEFAULT NULL,
  `ObservationSiteID` int(11) DEFAULT NULL,
  `AssistDate` date DEFAULT NULL,
  `AssistSig` tinyint(4) DEFAULT NULL,
  `AssistSiteID` int(11) DEFAULT NULL,
  `EvalID` int(11) NOT NULL,
  `StudentSig` varchar(255) DEFAULT NULL,
  `Consistent` tinyint(1) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Pass` tinyint(1) DEFAULT NULL,
  `ProficiencyExam` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`FormID`),
  UNIQUE KEY `FormID_UNIQUE` (`FormID`),
  KEY `fk_ObsSite` (`ObservationSiteID`),
  KEY `fk_AssistSite` (`AssistSiteID`),
  KEY `fk_Student_idx` (`StudID`),
  KEY `fk_Site_idx` (`SiteID`,`AssistSiteID`,`ObservationSiteID`),
  KEY `fk_Eval_idx` (`EvalID`),
  KEY `fk_Procedure_idx` (`ProcID`),
  CONSTRAINT `fk_AssistSite` FOREIGN KEY (`AssistSiteID`) REFERENCES `ClinicalSite` (`SiteID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Eval` FOREIGN KEY (`EvalID`) REFERENCES `Eval` (`EvalID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ObsSite` FOREIGN KEY (`ObservationSiteID`) REFERENCES `ClinicalSite` (`SiteID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Procedure` FOREIGN KEY (`ProcID`) REFERENCES `Proc` (`ProcedureID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Site` FOREIGN KEY (`SiteID`) REFERENCES `ClinicalSite` (`SiteID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Student` FOREIGN KEY (`StudID`) REFERENCES `Student` (`StudentID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GreenForm`
--

LOCK TABLES `GreenForm` WRITE;
/*!40000 ALTER TABLE `GreenForm` DISABLE KEYS */;
INSERT INTO `GreenForm` VALUES (1,1,2,'2018-09-05',1,'2018-09-03',1,1,'2018-09-03',1,1,1,'1',1,'Blah',0,0),(2,1,4,'2018-09-04',4,'2018-08-27',1,8,'2018-08-27',1,8,1,'1',1,'Blah',0,0),(3,1,1,'2018-08-28',1,'2018-09-10',1,1,'2018-09-11',1,1,1,'1',1,'Blah',0,0),(4,3,1,'1995-10-01',1,'1995-10-02',1,1,'1995-10-02',1,1,1,'1',0,'going?',1,0),(5,9,1,'2018-09-02',1,'2018-09-25',1,1,'2018-09-19',1,1,1,'1',1,'Blah',1,0),(6,9,1,'2018-09-02',1,'2018-09-25',1,1,'2018-09-19',1,1,1,'1',1,'Blah',1,0),(7,9,1,'2018-09-12',1,'2018-09-12',1,1,'2018-09-12',1,1,1,'1',0,'Blah',0,0),(8,3,7,'1994-01-11',1,'1994-01-11',1,1,'1994-01-12',1,1,1,'1',1,'year',1,0),(9,9,5,'2000-12-31',1,'2000-12-31',1,1,'2001-01-01',1,1,63,'1',0,'was he?',1,0),(10,1,8,'1994-03-13',1,'1994-03-13',1,1,'1994-03-14',1,1,64,'1',1,'checking on testie',1,0);
/*!40000 ALTER TABLE `GreenForm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Proc`
--

DROP TABLE IF EXISTS `Proc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Proc` (
  `ProcedureID` int(11) NOT NULL AUTO_INCREMENT,
  `ProcedureName` varchar(255) NOT NULL,
  `ProcedureType` varchar(1) NOT NULL,
  PRIMARY KEY (`ProcedureID`),
  UNIQUE KEY `ProcedureID_UNIQUE` (`ProcedureID`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Proc`
--

LOCK TABLES `Proc` WRITE;
/*!40000 ALTER TABLE `Proc` DISABLE KEYS */;
INSERT INTO `Proc` VALUES (1,'Chest Routine','M'),(2,'Chest AP (Wheelchair or Stretcher','M'),(3,'Ribs','M'),(4,'Chest Lateral Decubitus','E'),(5,'Sternum','E'),(6,'Upper Airway (Soft-Tissue Neck','E'),(7,'Thumb or Finger','M'),(8,'Hand','M'),(9,'Wrist','M'),(10,'Forearm','M'),(11,'Elbow','M'),(12,'Humerus','M'),(13,'Shoulder','M'),(14,'Trauma: Shoulder or Humerus (Scapular Y, Transthoraric, or Axial)','M'),(15,'Clavicle','M'),(16,'Scapula','E'),(17,'AC Joints','E'),(18,'Trauma: Upper Extremity(Non Shoulder)','M'),(19,'Toes','E'),(20,'Foot','M'),(21,'Ankle','M'),(22,'Knee','M'),(23,'Tibia-Fibula','M'),(24,'Femur','M'),(25,'Trauma: Lower Extremity','M'),(26,'Patella','E'),(27,'Calcaneus','E'),(28,'Skull','E'),(29,'Paranasal Sinuses','E'),(30,'Facial Bones','E'),(31,'Orbits','E'),(32,'Zygomatic Arches','E'),(33,'Mandible','E'),(34,'Temporomandibular Joints','E'),(35,'Cervical Spine','M'),(36,'Thoracic Spine','M'),(37,'Lumbar Spine','M'),(38,'Cross-Table (Horizontal Beam) Lateral Spine','M'),(39,'Pelvis','M'),(40,'Hip','M'),(41,'Cross-Table (Horizontal Beam) Lateral Hip','M'),(42,'Sacrum and/or Coccyx','E'),(43,'Scoliosis Series','E'),(44,'Sacroiliac Joints','E'),(45,'Abdomen Supine (KUB)','M'),(46,'Abdomen Upright','M'),(47,'Abdomen Decubitus','E'),(48,'Intravenous Urography','E'),(49,'Upper GI Series, Single or Double Contrast','E'),(50,'Contrast Enema, Single or Double Contrast','E'),(51,'Small Bowel Series','E'),(52,'Esophagus','E'),(53,'Cystopraphy/Cystourethrography','E'),(54,'ERCP','E'),(55,'Myelography','E'),(56,'Arthrography','E'),(57,'Hysterosalpingography','E'),(58,'C-Arm Procedure (Requiring Manipulation to Obtain More Than One Projection)','M'),(59,'Surgical C-Arm Procedure (Requiring Manipulation Around a Sterile Field)','M'),(60,'Mobile Chest','M'),(61,'Mobile Abdomen','M'),(62,'Mobile Orthopedic','M'),(63,'Pediatric Chest Routine','M'),(64,'Pediatric Upper Extremity','E'),(65,'Pediatric Lower Extremity','E'),(66,'Pediatric Abdomen','E'),(67,'Pediatric Mobile Study','E'),(68,'Geriatric Chest Routine','M'),(69,'Geriatric Upper Extremity','M'),(70,'Geriatric Lower Extremity','M');
/*!40000 ALTER TABLE `Proc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Student`
--

DROP TABLE IF EXISTS `Student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Student` (
  `StudentID` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(45) NOT NULL,
  `MiddleInitial` varchar(5) DEFAULT NULL,
  `LastName` varchar(255) NOT NULL,
  `ECPIID` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `StartDate` date NOT NULL,
  `GradDate` date NOT NULL,
  `Cohort` int(2) NOT NULL,
  `FirstLogin` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`StudentID`),
  UNIQUE KEY `StudentID_UNIQUE` (`StudentID`),
  UNIQUE KEY `ECPIID_UNIQUE` (`ECPIID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Student`
--

LOCK TABLES `Student` WRITE;
/*!40000 ALTER TABLE `Student` DISABLE KEYS */;
INSERT INTO `Student` VALUES (1,'Test','ing','Student','blabla5555','$2y$10$ZWDQ9oeHdPQ9WE899ua1A.c3LOW/THsPtT6dAX7X/7R.kqmm1o7dS','bremur5028@students.ecpi.edu','1998-10-05','1998-10-08',0,0),(2,'Beep','B','Beep','beepbeep1234','$2y$10$JjvOms5rnuVCiZwVCGR9lO1K/rgg.j/X3t62HqTiS8UeLjA3nHiMa','fun@fun.com','2018-09-03','2019-09-09',0,1),(3,'Zachary','T','Quinn','zacqui1234','MCIStu    dent','zacqui1234@students.ecpi.edu','2018-08-27','2018-09-29',1,1),(9,'Thomas','P','Sullivan','thosul1234','MCIStudent','thosul1234@students.ecpi.edu','1969-01-01','2018-12-31',1,1);
/*!40000 ALTER TABLE `Student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `UserNumber` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(45) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Admin` tinyint(4) DEFAULT NULL,
  `Clinician` tinyint(4) DEFAULT NULL,
  `FirstLogin` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`UserNumber`),
  UNIQUE KEY `UserNumber_UNIQUE` (`UserNumber`),
  UNIQUE KEY `UserID_UNIQUE` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'admin','$2y$10$mYtkU71AhpdY4mtUP6xnZOj2wp4Klo0r90vaHoLTmJRHvKCm6vQEa','admin@admin.com',1,0,0),(2,'clinician','$2y$10$xheH91bbwScEVYglo7I7eOw1bAPRF8e3IORIOAKV2af7Iie2zp0IK','clinic@clinic.com',0,1,0);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__bookmark`
--

DROP TABLE IF EXISTS `pma__bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__bookmark` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__bookmark`
--

LOCK TABLES `pma__bookmark` WRITE;
/*!40000 ALTER TABLE `pma__bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__central_columns`
--

DROP TABLE IF EXISTS `pma__central_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin,
  PRIMARY KEY (`db_name`,`col_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__central_columns`
--

LOCK TABLES `pma__central_columns` WRITE;
/*!40000 ALTER TABLE `pma__central_columns` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__central_columns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__column_info`
--

DROP TABLE IF EXISTS `pma__column_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__column_info` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__column_info`
--

LOCK TABLES `pma__column_info` WRITE;
/*!40000 ALTER TABLE `pma__column_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__column_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__designer_settings`
--

DROP TABLE IF EXISTS `pma__designer_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__designer_settings`
--

LOCK TABLES `pma__designer_settings` WRITE;
/*!40000 ALTER TABLE `pma__designer_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__designer_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__export_templates`
--

DROP TABLE IF EXISTS `pma__export_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__export_templates` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__export_templates`
--

LOCK TABLES `pma__export_templates` WRITE;
/*!40000 ALTER TABLE `pma__export_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__export_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__favorite`
--

DROP TABLE IF EXISTS `pma__favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__favorite`
--

LOCK TABLES `pma__favorite` WRITE;
/*!40000 ALTER TABLE `pma__favorite` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__history`
--

DROP TABLE IF EXISTS `pma__history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`,`db`,`table`,`timevalue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__history`
--

LOCK TABLES `pma__history` WRITE;
/*!40000 ALTER TABLE `pma__history` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__navigationhiding`
--

DROP TABLE IF EXISTS `pma__navigationhiding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__navigationhiding`
--

LOCK TABLES `pma__navigationhiding` WRITE;
/*!40000 ALTER TABLE `pma__navigationhiding` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__navigationhiding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__pdf_pages`
--

DROP TABLE IF EXISTS `pma__pdf_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`page_nr`),
  KEY `db_name` (`db_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__pdf_pages`
--

LOCK TABLES `pma__pdf_pages` WRITE;
/*!40000 ALTER TABLE `pma__pdf_pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__pdf_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__recent`
--

DROP TABLE IF EXISTS `pma__recent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__recent`
--

LOCK TABLES `pma__recent` WRITE;
/*!40000 ALTER TABLE `pma__recent` DISABLE KEYS */;
INSERT INTO `pma__recent` VALUES ('richard','[{\"db\":\"Test\",\"table\":\"Student\"},{\"db\":\"Test\",\"table\":\"GreenForm\"},{\"db\":\"Test\",\"table\":\"Proc\"},{\"db\":\"Test\",\"table\":\"Eval\"},{\"db\":\"Test\",\"table\":\"ClinicalSite\"},{\"db\":\"Test\",\"table\":\"Users\"}]');
/*!40000 ALTER TABLE `pma__recent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__relation`
--

DROP TABLE IF EXISTS `pma__relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  KEY `foreign_field` (`foreign_db`,`foreign_table`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__relation`
--

LOCK TABLES `pma__relation` WRITE;
/*!40000 ALTER TABLE `pma__relation` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__savedsearches`
--

DROP TABLE IF EXISTS `pma__savedsearches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__savedsearches` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__savedsearches`
--

LOCK TABLES `pma__savedsearches` WRITE;
/*!40000 ALTER TABLE `pma__savedsearches` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__savedsearches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__table_coords`
--

DROP TABLE IF EXISTS `pma__table_coords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float unsigned NOT NULL DEFAULT '0',
  `y` float unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__table_coords`
--

LOCK TABLES `pma__table_coords` WRITE;
/*!40000 ALTER TABLE `pma__table_coords` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__table_coords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__table_info`
--

DROP TABLE IF EXISTS `pma__table_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`db_name`,`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__table_info`
--

LOCK TABLES `pma__table_info` WRITE;
/*!40000 ALTER TABLE `pma__table_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__table_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__table_uiprefs`
--

DROP TABLE IF EXISTS `pma__table_uiprefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`,`db_name`,`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__table_uiprefs`
--

LOCK TABLES `pma__table_uiprefs` WRITE;
/*!40000 ALTER TABLE `pma__table_uiprefs` DISABLE KEYS */;
INSERT INTO `pma__table_uiprefs` VALUES ('richard','Test','Student','{\"sorted_col\":\"`Student`.`FirstLogin`  DESC\"}','2018-09-20 14:05:12');
/*!40000 ALTER TABLE `pma__table_uiprefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__tracking`
--

DROP TABLE IF EXISTS `pma__tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`db_name`,`table_name`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__tracking`
--

LOCK TABLES `pma__tracking` WRITE;
/*!40000 ALTER TABLE `pma__tracking` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__userconfig`
--

DROP TABLE IF EXISTS `pma__userconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__userconfig`
--

LOCK TABLES `pma__userconfig` WRITE;
/*!40000 ALTER TABLE `pma__userconfig` DISABLE KEYS */;
INSERT INTO `pma__userconfig` VALUES ('richard','2018-09-20 14:04:00','{\"Console\\/Mode\":\"collapse\"}');
/*!40000 ALTER TABLE `pma__userconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__usergroups`
--

DROP TABLE IF EXISTS `pma__usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`usergroup`,`tab`,`allowed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__usergroups`
--

LOCK TABLES `pma__usergroups` WRITE;
/*!40000 ALTER TABLE `pma__usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__users`
--

DROP TABLE IF EXISTS `pma__users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`username`,`usergroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__users`
--

LOCK TABLES `pma__users` WRITE;
/*!40000 ALTER TABLE `pma__users` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-28 14:50:55
