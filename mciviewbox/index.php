<!DOCTYPE html>

<!--
  TODO:
1.) Fix Hamburg menu X
 -->
 <?php
session_start();
session_unset();

session_destroy();



?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>MCI ViewBox</title>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="http://mciviewbox.ddns.net/" style="color:#C70A0D;">MCI ViewBox</a>

</nav>
<!-- END NAV BAR -->

<!-- BEGIN LOGIN FORM -->
<div class="jumbotron">

<div class="row">
<div class="col-3">

</div>


<div class="col-6 ">
<h1 class="display-4 text-center">Login</h1>
<form method="POST" action="<?= htmlentities('verify.php');?>">
<div class="form-group">

    <div class="input-group shadow">
  <div class="input-group-prepend">
    <span class="input-group-text">Username</span>
  </div>
  <input type="text" class="form-control" name="username">
</div>
<br>
<div class="input-group  shadow">
<div class="input-group-prepend">
<span class="input-group-text">Password</span>
</div>
<input type="password" class="form-control" name="password">

</div>
<button type="submit" class="btn btn-primary mt-3 float-sm-right">Login</button>

<button type="submit" name="reset" class="btn btn-secondary mt-3 float-sm-left">Reset Password</button>
<br>
<br>
<br>
<div style="margin: auto; width: 50%; text-align: center">
<div class="alert alert-danger h-20 <?php
if($_GET["check"] == '') {
  echo "text-hide";
}

?>" role="alert" id="passwordFailed">
  <strong>Login Failed :(</strong> Change a few things up and try submitting again.
</div>
<div style="margin: auto; width: 100%; text-align: center;">
<div class="alert alert-success h-20 <?php
if($_GET["newUser"] == '1') {
   echo "";
}
else {
   echo "text-hide";
}


?>" role="alert" id="newUser">
	<strong>Please Check ECPI Email For Temporary Password</strong>
</div>
</div>
</div>

</form>

</div>
</div> <!-- END LOGIN BARS -->


<div class="col-3">

</div>


</div> <!-- END INPUT ROW -->
</div> <!-- END LOGIN WINDOW -->
  </body>
</html>
