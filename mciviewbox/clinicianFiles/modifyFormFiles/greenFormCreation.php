<?php
	session_start();
	require '../../dbconn.php';
	
	$Stage = $_SESSION['Stage'];

    	$stud = $_SESSION['StudentName'];
    	$proc = $_SESSION['ProcName'];

    	$getStudID=mysqli_query($conn, "SELECT StudentID FROM Student WHERE CONCAT(FirstName, ' ', LastName) = '$stud'");
    	$studRow2 = mysqli_fetch_array($getStudID);
   	$StudID = $studRow2['StudentID'];

    	$getProcID=mysqli_query($conn, "SELECT ProcedureID FROM Proc WHERE ProcedureName = '$proc'");
    	$row2 = mysqli_fetch_array($getProcID);
    	$ProcID = $row2['ProcedureID'];


	if($Stage == 'obs') {
    		$obsDate = date("Y-m-d", strtotime($_POST['obsDate']));
    		$obsPrint = $_POST['obsPrint'];
    		$obsSite = $_POST['obsSite'];

    		$getObsSiteID = mysqli_query($conn, "SELECT SiteID FROM ClinicalSite WHERE CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) = '$obsSite'");
    		$obsSiteRow2 = mysqli_fetch_array($getObsSiteID);
    		$obsSiteID = $obsSiteRow2['SiteID'];

    		$dataUrl = $_POST['obsData'];
    		$rawFileName = $stud.$proc."ObservationSig.png";
    		$fileName = str_replace(' ', '', $rawFileName);
    		$obsSigLocation = "../../signatures/".$fileName;
    		$encodedImage = explode(",", $dataUrl)[1];
		$decodedImage = base64_decode($encodedImage);
    		file_put_contents($obsSigLocation, $decodedImage);

    		$exist = mysqli_query($conn, "SELECT * FROM GreenForm WHERE StudID = '$StudID' AND ProcID = '$ProcID'");

	    if(mysqli_num_rows($exist) > 0) {
		    $update = mysqli_query($conn, "UPDATE GreenForm SET ObservationDate = '$obsDate', ObservationPrint = '$obsPrint', ObservationSig = '$obsSigLocation', ObservationSiteID = '$obsSiteID' WHERE StudID = '$StudID' AND ProcID = '$ProcID'");
	    }
	    else {
		    $insert = mysqli_query($conn, "INSERT INTO GreenForm(StudID, ProcID, ObservationDate, ObservationPrint, ObservationSig, ObservationSiteID) VALUES ('$StudID', '$ProcID', '$obsDate', '$obsPrint', '$obsSigLocation', '$obsSiteID')");
	    }
		sleep(1);
		header('Location: http://mciviewbox.ddns.net/clinicianPage.php');
	}
	else if($Stage == 'assist') {
		$assDate = date("Y-m-d", strtotime($_POST['assDate']));
    		$assistPrint = $_POST['assistPrint'];
    		$assSite = $_POST['assSite'];

    		$getAssSiteID = mysqli_query($conn, "SELECT SiteID FROM ClinicalSite WHERE CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) = '$assSite'");
    		$assSiteRow2 = mysqli_fetch_array($getAssSiteID);
		$assSiteID = $assSiteRow2['SiteID'];

    		$dataUrl = $_POST['assData'];
    		$rawFileName = $stud.$proc."AssistSig.png";
    		$fileName = str_replace(' ', '', $rawFileName);
    		$assistSigLocation = "../../signatures/".$fileName;
    		$encodedImage = explode(",", $dataUrl)[1];
    		$decodedImage = base64_decode($encodedImage);
    		file_put_contents($assistSigLocation, $decodedImage);

    $exist = mysqli_query($conn, "SELECT * FROM GreenForm WHERE StudID = '$StudID' AND ProcID = '$ProcID'");

    if(mysqli_num_rows($exist)) {

		    $update = mysqli_query($conn, "UPDATE GreenForm SET AssistDate = '$assDate', AssistPrint = '$assistPrint', AssistSig = '$assistSigLocation', AssistSiteID = '$assSiteID' WHERE StudID = '$StudID' AND ProcID = '$ProcID'");
	    }
    else {
		    $insert = mysqli_query($conn, "INSERT INTO GreenForm(StudID, ProcID, AssistDate, AssistPrint, AssistSig, AssistSiteID) VALUES ('$StudID', '$ProcID', '$assDate', '$assistPrint', '$assistSigLocation', '$assSiteID')");
	    }
		sleep(1);
		header('Location: http://mciviewbox.ddns.net/clinicianPage.php');
	}
	else if($Stage == 'eval') {
		$compDate = date("Y-m-d", strtotime($_POST['compDate']));
    $site = $_POST['site'];
    $evalAnatomy1 = $_POST['bodypart1'];
    $evalAnatomy2 = $_POST['bodypart2'];
    $evalAnatomy3 = $_POST['bodypart3'];
    $evalComments = $_POST['evalComments'];
    $clinName = $_POST['clinicalPrint'];
    $consistent = $_POST['consistent'];
    $proficient = $_POST['proficiency'];
    $comments = $_POST['comments'];


    $getSiteID = mysqli_query($conn, "SELECT SiteID FROM ClinicalSite WHERE CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) = '$site'");
    $siteRow2 = mysqli_fetch_array($getSiteID);
    $SiteID = $siteRow2['SiteID'];

	$dataUrl = $_POST['studData'];
	$rawFileName = $stud.$proc."StudentSig.png";
	$fileName = str_replace(' ', '', $rawFileName);
	$encodedImage = explode(",", $dataUrl)[1];
	$decodedImage = base64_decode($encodedImage);
	$studSigLocation = "../../signatures/".$fileName;
	file_put_contents($studSigLocation, $decodedImage);

/*	$clindataUrl = $_POST['clinData'];
	$clinrawFileName = $clinName.$proc."InstructorSig.png";
	$clinFileName = str_replace(' ', '', $clinrawFileName);
	$clinencodedImage = explode(",", $clindataUrl, 1);
	$clindecodedImage = base64_decode($clinencodedImage);
	$cliSigLocation = "../../signatures/".$clinFileName;
	file_put_contents("../../signatures/".$clinFileName, $clindecodedImage);
*/
    if($consistent == 'yes') {
	$consistent = 1;
    }
    else {
	$consistent = 0;
    }

    if($proficient == 'yes') {
	$proficient = 1;
    }
    else {
	$proficient = 0;
    }

    $EvalCount = '';

    for($count = 1; $count < 26; $count++)
    {
	    if($count == 25) {
		    $EvalCount .= '`'.$count.'`';
	    }
	    else {
		    $EvalCount .= '`'.$count.'`, ';
	    }
    }

		    $EvalIDGFUpd = mysqli_query($conn, "SELECT EvalID FROM GreenForm WHERE StudID = '$StudID' AND ProcID = '$ProcID'");
		    $EvalIDGFURow = mysqli_fetch_array($EvalIDGFUpd);
		    $EvalIDGFU = $EvalIDGFURow['EvalID'];
	    $exist = mysqli_query($conn, "SELECT * FROM GreenForm WHERE StudID = '$StudID' AND ProcID = '$ProcID'");
	    if(mysqli_num_rows($exist) > 0 && $EvalIDGFU != 'NULL' && $EvalIDGFU != '')
	    {
		    $EvalTotalUpdate = 0;
    		    for($count = 1; $count < 26; $count++) {
	    		$curPost = 'options'.$count;
	    		$EvalUpdateBtn=$_POST[$curPost];

	    		if(isset($EvalUpdateBtn))
	    		{
		    		if($count == 25)
		    		{
		    			if($EvalUpdateBtn == 'yes')
		    			{
						$EvalUpdateEntry .= '`'.$count.'` = '. 4;
						$EvalTotalUpdate += 4;
		    			}
		    			else if($EvalUpdateBtn == 'no')
		    			{
						$EvalUpdateEntry .= '`'. $count .'` = '. 0;
		    			}
		    		}
		    		else
		    		{
					if($EvalUpdateBtn == 'yes')
		      			{
						$EvalUpdateEntry .= '`'.$count.'` = '. 4 . ', ';
						$EvalTotalUpdate += 4;
		    			}
		    			else if($EvalUpdateBtn == 'no')
		    			{
						$EvalUpdateEntry .= '`'.$count.'` = '. 0 . ', ';
					}

		    		}

	    		}
    		    }
############################################################################################################################################################################ UPDATING EVAL TABLE #######################################################################################################################################################################################################################################
		    $EvalUpd = mysqli_query($conn, "UPDATE Eval SET $EvalUpdateEntry, Anatomy1 = '$evalAnatomy1', Anatomy2 = '$evalAnatomy2', Anatomy3 = '$evalAnatomy3', Comments = '$evalComments' WHERE Eval.EvalID = (SELECT GreenForm.EvalID FROM GreenForm WHERE StudID = '$StudID' AND ProcID = '$ProcID')");
                    ############################################################################################################################################ CHECKING FOR A PASSING SCORE ####################################################################################################################################################
		    if($EvalTotalUpdate > 73) {
			 $Pass = 1;
		    }
		    else {
		    	 $Pass = 0;
		    }

		    ################################################################################################################################################ UPDATING GREEN FORM ##########################################################################################################################################################
		    $update = mysqli_query($conn, "UPDATE GreenForm SET CompDate = '$compDate', SiteID = '$SiteID', EvalID = '$EvalIDGFU', StudentSig = '$studSigLocation', Consistent = '$consistent', Comments = '$comments', Pass = '$Pass', ProficiencyExam = '$proficient' WHERE StudID = '$StudID' AND ProcID = '$ProcID'");
	    }
	    else if(mysqli_num_rows($exist) > 0 && ($EvalIDGFU == '' || $EvalIDGFU == 'NULL')) {
		$EvalTotal = 0;
    		    for($count=1; $count<26; $count++)
    			{
	    			$curPost = 'options'.$count;
	    			$EvalBtn=$_POST[$curPost];

	    			if(isset($EvalBtn))
	    			{
		    			if($count == 25)
		    			{
		    				if($EvalBtn == 'yes')
		    				{
							$EvalEntry .= 4;
							$EvalTotal += 4;
		    				}
		    				else if($EvalBtn == 'no')
		    				{
							$EvalEntry .= 0;
		    				}
		    			}
		    			else
		    			{
						if($EvalBtn == 'yes')
		      				{
							$EvalEntry .= 4 . ', ';
							$EvalTotal += 4;
		    				}
		    				else if($EvalBtn == 'no')
		    				{
							$EvalEntry .= 0 . ', ';
						}

		    			}

	    			}
    			}
		    ############################################################################################################################################### INSERTING INTO EVAL TABLE #######################################################################################################################################################
		    $EvalIns = mysqli_query($conn,  "INSERT INTO Eval($EvalCount, Anatomy1, Anatomy2, Anatomy3, Comments) VALUES($EvalEntry, '$evalAnatomy1', '$evalAnatomy2', '$evalAnatomy3', '$evalComments')");

    		    $getEvalID = mysqli_query($conn, "SELECT MAX(EvalID) AS EvalID FROM Eval");
    		    $getEvalIDRow = mysqli_fetch_array($getEvalID);
    		    $MaxEvalID = $getEvalIDRow['EvalID'];

    		    if($EvalTotal >= 73) {
		   	 $Pass = 1;
    		    }
    		    else {
			 $Pass = 0;
    		    }
		    $update = mysqli_query($conn, "UPDATE GreenForm SET CompDate = '$compDate', SiteID = '$SiteID', EvalID = '$MaxEvalID', StudentSig = '$studSigLocation', Consistent = '$consistent', Comments = '$comments', Pass = '$Pass', ProficiencyExam = '$proficient' WHERE StudID = '$StudID' AND ProcID = '$ProcID'");
	    }

	    else {
		$EvalTotal = 0;
    		    for($count=1; $count<26; $count++)
    			{
	    			$curPost = 'options'.$count;
	    			$EvalBtn=$_POST[$curPost];

	    			if(isset($EvalBtn))
	    			{
		    			if($count == 25)
		    			{
		    				if($EvalBtn == 'yes')
		    				{
							$EvalEntry .= 4;
							$EvalTotal += 4;
		    				}
		    				else if($EvalBtn == 'no')
		    				{
							$EvalEntry .= 0;
		    				}
		    			}
		    			else
		    			{
						if($EvalBtn == 'yes')
		      				{
							$EvalEntry .= 4 . ', ';
							$EvalTotal += 4;
		    				}
		    				else if($EvalBtn == 'no')
		    				{
							$EvalEntry .= 0 . ', ';
						}

		    			}

	    			}
    			}
		    ############################################################################################################################################### INSERTING INTO EVAL TABLE #######################################################################################################################################################
		    $EvalIns = mysqli_query($conn,  "INSERT INTO Eval($EvalCount, Anatomy1, Anatomy2, Anatomy3, Comments) VALUES($EvalEntry, '$evalAnatomy1', '$evalAnatomy2', '$evalAnatomy3', '$evalComments')");

    		    $getEvalID = mysqli_query($conn, "SELECT MAX(EvalID) AS EvalID FROM Eval");
    		    $getEvalIDRow = mysqli_fetch_array($getEvalID);
    		    $MaxEvalID = $getEvalIDRow['EvalID'];

    		    if($EvalTotal >= 73) {
		   	 $Pass = 1;
    		    }
    		    else {
			 $Pass = 0;
    		    }
		    $insert = mysqli_query($conn, "INSERT INTO GreenForm(StudID, ProcID, CompDate, SiteID, EvalID, StudentSig, Consistent, Comments, Pass, ProficiencyExam) VALUES ('$StudID', '$ProcID', '$compDate', '$SiteID', $MaxEvalID, '$studSigLocation', '$consistent', '$comments', '$Pass', '$proficient')");
echo "INSERT INTO GreenForm(StudID, ProcID, CompDate, SiteID, EvalID, StudentSig, Consistent, Comments, Pass, ProficiencyExam) VALUES ('$StudID', '$ProcID', '$compDate', '$SiteID', $MaxEvalID, '$studSigLocation', '$consistent', '$comments', '$Pass', '$proficient')";
	    }
		sleep(1);
		header('Location: http://mciviewbox.ddns.net/clinicianPage.php');
	}
	else {
		# BRENNEN INSERT ALERT BOX THINGY HERE
	}
?>
	
