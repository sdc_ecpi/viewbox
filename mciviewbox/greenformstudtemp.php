 <?php
  session_start();

  #if(!isset($_SESSION['username'])) {
   # header('Location: http://mciviewbox.ddns.net');
   # die();
  #}


 ?>



 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title>Green Form Template</title>
     <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
     <style media="screen">
       .table-borderless td, .table-borderless th {
         border: none;
       }

       input, textarea {
	 cursor: default;
       }
     </style>





   </head>
   <body>
	<?php
		require 'dbconn.php';
		$procedure = $_POST['procs'];
		$StudAndProc="StudID = (SELECT StudentID FROM Student WHERE CONCAT(FirstName, ' ', LastName) = '".$_SESSION['Student']."') AND ProcID = (SELECT ProcedureID FROM Proc WHERE ProcedureName = '$procedure')";
		$EvalInfo=mysqli_query($conn, "SELECT `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `10`, `11`, `12`, `13`, `14`, `15`, `16`, `17`, `18`, `19`, `20`, `21`, `22`, `23`, `24`, `25` FROM Eval JOIN GreenForm ON Eval.EvalID = GreenForm.EvalID WHERE $StudAndProc");
    		$EvalArray=mysqli_fetch_array($EvalInfo); 
	?>
     <!-- style="background-color: #d7eddd"    #61b579-->
     <!-- nav bar with editing commands and buttons -->
    <div class="container container-fluid mt-2 mb-5 shadow-lg" style="background-color: #5ec177"> <!--begin transcribe from green form-->
    <form class="pt-2" action="<?php $_SERVER['PHP_SELF'];?>" method="post" >
     <div class="form-group">


       <div class="row" id="Univ-header">
         <div class="col-sm-10 offset-sm-1">
           <p class="text-center" style="font-weight: bolder;">MEDICAL CAREERS INSTITUTE COLLEGE OF HEALTH SCIENCE OF ECPI UNIVERSITY MEDICAL RADIOLOGY PROGRAM</p>
           <br>
           <h1 class="lead text-center">CLINICAL COMPETENCY EVALUATION FORM</h1> <!-- BEGIN FIRST SUBHEADING -->

         </div>
         <div class="col-sm-2">

         </div>
         <h4 ></h4>

     </div> <!-- END UNIV HEADER -->
           <br>
      <div class="row" id="form-meta-info"> <!-- FORM META INFO ROW 1 -->
        <div class="col-sm-6 ">
          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">Student Name</span>
	    </div>

	        <input name="student" value="<?php
					      $StudNames=mysqli_query($conn, "SELECT CONCAT(FirstName, ' ', LastName) AS Name FROM Student WHERE CONCAT(FirstName, ' ', LastName) = '".$_SESSION['Student']."'");
					      $StudInfo=mysqli_fetch_array($StudNames);
					      echo $StudInfo[0];
                                              ?>" class="form-control" disabled>
          </div>
        </div>
        <div class="col-sm-6">
          <!-- QUERY DATABASE FOR PROCEDURES -->
          <div class="input-group input-group-sm ">
            <div class="input-group-prepend">
              <span class="input-group-text">Procedure</span>
            </div>

		<input name="procedure" value="<?php
						$ProcNames=mysqli_query($conn, "SELECT ProcedureName FROM Proc WHERE ProcedureName = '$procedure'");
						$ProcInfo=mysqli_fetch_array($ProcNames);
						echo $ProcInfo['ProcedureName'];
						?>" class="form-control" disabled>

          </div>
        </div>
      </div> <!-- END FORM META INFO ROW 1-->
        <br>
        <div class="row">  <!-- BEGIN FORM META INFO ROW 2 -->
          <div class="col-sm-7">
            <div class="input-group input-group-sm">
              <div class="input-group-prepend">
                <span class="input-group-text">Competency Date</span>
	      </div>

              <input name="compDate" value="<?php
						$getCompDate=mysqli_query($conn, "SELECT CompDate FROM GreenForm WHERE $StudAndProc");
						$compDate = mysqli_fetch_array($getCompDate);
					if($compDate['CompDate'] != '') {
						$convertCompDate = date("m/d/Y", strtotime($compDate['CompDate']));
						echo $convertCompDate;
					}
					else {
						echo "N/A";
					}
			?>" class="form-control" disabled>
            </div>
          </div>

          <div class="col-sm-5">

              <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                  <span class="input-group-text">Clinical Site</span>
                </div>

		<input name="clinicalSite" value="<?php
						   $SiteNames=mysqli_query($conn, "SELECT CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) AS Site FROM ClinicalSite JOIN GreenForm ON ClinicalSite.SiteID = GreenForm.SiteID WHERE $StudAndProc");
						   $SiteInfo=mysqli_fetch_array($SiteNames);
						if($SiteInfo['Site'] != '') {
						   echo $SiteInfo['Site'];
						}
						else {
						   echo "N/A"; 
						} ?>" class="form-control" disabled>

            </div>
          </div>

        </div> <!-- END FORM META INFO ROW 2 -->
        <br>
        <h2 class="lead text-center">DOCUMENTATION by R.T (R)</h2> <!-- BEGIN SECOND HEADING -->
        <div class="row"> <!-- BEGIN DOCUMENTATION TABLE -->


          <table class="table table-borderless ">


              <tr class="shadow-sm text-center " style="background-color: #00ad6a;">
                <th></th>
                <th>Date</th>
                <th>Print</th>
                <th>Signature</th>
                <th>Site</th>
              </tr>



              <tr>
                <th scope="row">1: Observation</th>
                <td><input name="obsDate" value="<?php
						$getObsDate=mysqli_query($conn, "SELECT ObservationDate FROM GreenForm WHERE $StudAndProc");
						$ObsDate = mysqli_fetch_array($getObsDate);
					if($ObsDate['ObservationDate'] != '') {
						$convertObsDate = date("m/d/Y", strtotime($ObsDate['ObservationDate']));
						echo $convertObsDate;
					}
					else {
						echo "N/A";
					} ?>" class="form-control disabled form-control-sm" disabled></td>
                <td><input type="text" class="form-control form-control-sm" value="<?php
				$getObsPrint=mysqli_query($conn, "SELECT ObservationPrint FROM GreenForm WHERE $StudAndProc");
				$ObsPrint = mysqli_fetch_array($getObsPrint);
			if($ObsPrint['ObservationPrint'] != '') {
				echo $ObsPrint['ObservationPrint'];
			}
			else {
				echo "N/A";
			} ?>" disabled></td>
                <td witdh=20%><img src="<?php
				$getObsSigLocation=mysqli_query($conn, "SELECT ObservationSig FROM GreenForm WHERE $StudAndProc");
				$getObsSigLocationRow=mysqli_fetch_array($getObsSigLocation);
				$ObsSigLocation = $getObsSigLocationRow['ObservationSig']; 
				if($ObsSigLocation == '') {
					echo "signatures/default.png";
				}
				else {
					#echo "signatures/TestStudentChestRoutineObservationSig.png";
					echo $ObsSigLocation;
				}?>" width=100% />				
		</td>
		<td><input name="obsSite" value="<?php
							$ObsSite=mysqli_query($conn, "SELECT CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) AS Site FROM ClinicalSite JOIN GreenForm ON ClinicalSite.SiteID = GreenForm.ObservationSiteID WHERE $StudAndProc");
							$ObsSiteInfo=mysqli_fetch_array($ObsSite);
						if($ObsSiteInfo['Site'] != '') {
							echo $ObsSiteInfo['Site'];
						}
						else {
							echo "N/A";
						} ?>" class="form-control disabled form-control-sm" disabled></td>
              </tr>
              <tr>
                <th scope="row">2: Assisted</th>
                <td><input name="assDate" value="<?php
						$getAssDate=mysqli_query($conn, "SELECT AssistDate FROM GreenForm WHERE $StudAndProc");
						$AssDate = mysqli_fetch_array($getAssDate);
					if($AssDate['AssistDate'] != '') {
						$convertAssDate = date("m/d/Y", strtotime($AssDate['AssistDate']));
						echo $convertAssDate;
					}
					else {
						echo "N/A";
					} ?>" class="form-control form-control-sm" disabled></td>
                <td><input type="text" class="form-control form-control-sm" value="<?php
				$getAssistPrint=mysqli_query($conn, "SELECT AssistPrint FROM GreenForm WHERE $StudAndProc");
				$AssistPrint = mysqli_fetch_array($getAssistPrint);
			if($AssistPrint['AssistPrint'] != '') {
				echo $AssistPrint['AssistPrint'];
			}
			else {
				echo "N/A";
			} ?>"disabled></td>
                <td width=20% ><img src="<?php
				$getAssSigLocation=mysqli_query($conn, "SELECT AssistSig FROM GreenForm WHERE $StudAndProc");
				$getAssSigLocationRow=mysqli_fetch_array($getAssSigLocation);
				$AssSigLocation = $getAssSigLocationRow['AssistSig'];
				if ($AssSigLocation == '') {
					echo "signatures/default.png";
				}
				else {
					echo $AssSigLocation;
				}		?>" width=100% />
		</td>
                <td><input name="assSite" value="<?php
							$AssSite=mysqli_query($conn, "SELECT CONCAT(SiteName, ' ', SiteCity, ', ', SiteState) AS Site FROM ClinicalSite JOIN GreenForm ON ClinicalSite.SiteID = GreenForm.AssistSiteID WHERE $StudAndProc");
							$AssSiteInfo=mysqli_fetch_array($AssSite);
						if($AssSiteInfo['Site'] != '') {
							echo $AssSiteInfo['Site'];
						}
						else {
							echo "N/A";
						} ?>" class="form-control form-control-sm" disabled></td>
              </tr>

          </table>


        </div> <!-- END DOCUMENTATION TABLE -->
        <hr>

        <!-- STUDENT COMMENT IF REQUIRED-->
        <div class="row"> <!-- EVALUATION CRITERIA HEADING-->
        <div class="col-sm-4 offset-4">
          <h2 class="lead text-center">EVALUATION CRITERIA</h2>

        </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="text-center">

            <p><strong>Competency Requirement:</strong> Perform the Radiologic procedure  appropriately, utilizing these criteria:</p>
            <p><strong>There are 25 category criteria ("Yes" / "No")</strong></p>
            <p>All projections for a given examination must be performed correctly in order to recieve a "Yes" for the category.</p>
          </div>
          </div>
        </div>
      </div> <!-- END FORM GROUP -->
      <div class="row">

      <table class="table table-sm table-borderless table-hover"> <!-- BEGIN EVALUATION TABLE -->
        <tbody>

            <!--
            This section screams out to be generated in php. All of the below will be populated by the database, as these 25 criteria can change depending on the type of evaluation, etc.
            php will be rendering pretty much all of this, but here's the template to use. I'll adjust the sizing and spacing, don't worry about that.
           -->

           <?php

           $questionArray = array(
             "1"=>"Identifies patient using two patient identifiers",
             "2"=>"Verifies requisition is applicable according to anatomical part and history",
             "3"=>"Obtains patient's history; inquires about possible pregnancy (if applicable)",
             "4"=>"Cleans room/table/wall unit; linens, pillow, table pad etc.",
             "5"=>"Equipment readiness (Table, tube, girds, etc)",
             "6"=>"Accesories ready (i.e, Flouroscopy aprons, contrast media, etc.)",
             "7"=>"Communicates & assess patient throughout the exam (Respiration, appearance, etc.)",
             "8"=>"Provides patient assistance & comfort throughout the exam (transfer, positioning, etc)",
             "9"=>"Makes adjustments for medical Equipment: I.V / 02 / tubing/leads, etc. (if applicable)",
             "10"=>"Correct tube and Table Handling (SID, Locks, Detent, Movements & Angles)",
             "11"=>"Correct selection of Technical Factors: ",
             "12"=>"Correct Receptor Selection (Table top/bucky/IR alignment/ Size / Placement)",
             "13"=>"Performs the correct Projection/Position(s) for Anatomical Part (AP/PA/Lateral, etc.)",
             "14"=>"Correct Body Part alignment (Erect/Supine/Prone/Oblique/Lateral)",
             "15"=>"Correct central ray placement",
             "16"=>"Correct use of Anatomical Markers and Positioning Aids",
             "17"=>"Provides proper Protection (Sheilding / Collimation / Technique)",
             "18"=>"Ensure Self and Others are Protected (sheilding / barrier / Distance / Exposure Announcement)",
             "19"=>"Correctly inputs & verifies patient demographic information on workstation/ PACS",
             "20"=>"Correctly exposes and processes the image (processor/computer)",
             "21"=>"Images demonstrate accurate positioning & demonstrate all required anatomy",
             "22"=>"Images demonstrate acceptable density, contrast and collination",
             "23"=>"Images are free of artifacts, distortion, and motion, etc.",
             "24"=>"Completes exam in a timely manner & patient given follow-up instructions",
             "25"=>"Correctly identifies at least three(3) anatomical structures <em>(no partial credit allowed)</em>",
           );
	   for($iter = 1; $iter < 26; $iter++) {
		$i = $iter - 1;
             if($iter == 1) {
               echo '
                <tr scope="row" style="background-color: #00ad6a;" class="">
                  <th scope="row" class="">&nbspA. <span class="text-center" >Evaluation of Procedure Requisistion</span></th>
                  <td></td>';
             } else if($iter == 4) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" >
                  <th scope="row">&nbspB. Room Preparation</th>
                  <td></td>';
             } else if($iter == 7) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" >
                 <th scope="row" class="">&nbspC. Patient Assesment and Management</th>
                 <td></td>';
             } else if($iter == 10) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row" class="">&nbspD. Equipment Operation and Technique</th>
                 <td></td>';
             } else if($iter == 13) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspE. Positioning Skills</th>
                <td></td>';
             } else if($iter == 17) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspF. Radiation Safety</th>
                 <td></td>';
             } else if($iter == 19) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspG. Image Aquisition</th>
                 <td></td>';
             } else if($iter == 21) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspH. Image analysis and Examination Completion</th>
                 <td></td>';
             } else if($iter == 25) {
               echo '
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspI. Anatomical Parts Identification</th>
                 <td></td>';
             }
           echo '
           <tr>
            <td scope="row" class="pl-4 pt-3">' . $iter . '. <span class="">' . $questionArray[$iter] . '</span></td>
            <td>
	    <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">';


      //echo $EvalInfo[0];
	    $EvalAns=($EvalArray[$iter - 1]);
	    $Score = 0;

                     if($EvalAns == 4)
                     {
	    		echo '<label class="btn btn-info">
	      			<input type="radio" name="options' . $iter . '" value="yes' . $iter .'" autocomplete="off" disabled><span style=" font-weight: bold;">Yes</span>
            		</label>
            		<label class="btn btn-secondary disabled">
	      			<input type="radio" name="options' . $iter . '" value="no' . $iter .'" autocomplete="off" disabled> <span style=" font-weight: bold;">No</span>
			</label>';
			$Score += 4;
		    }
		     else if($EvalAns == 0)
		     {
			echo '<label class="btn btn-secondary disabled">
				<input type="radio" name="options' . $iter . '" value="yes' . $iter .'" autocomplete="off" disabled><span style=" font-weight: bold;">Yes</span>
            		</label>
            		<label class="btn btn-danger">
	      			<input type="radio" name="options' . $iter . '" value="no' . $iter .'" autocomplete="off" disabled> <span style=" font-weight: bold;">No</span>
			</label>';
			$Score += 0;
		     }

             echo '</div>
           </td>
         </tr>';}

          ?>


         </tr>
        </tbody>
      </table>
    </div> <!-- END EVALUATION SECTION -->
      <hr>
      <div class="row">
        <div class="col-sm-10 offset-1">

          <p class="text-center">Note: R.T (R) may have the student identify specific anatomy or ask the student to point to requested anatomy</p>
        </div>

      </div>
      <div class="row">
        <div class="col-sm-2">
          <div>
            <p><strong>List Anatomy:</strong></p>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">(1)</span>
            </div>
	    <input name="bodypart1" value="<?php
	   				$AnatomyInfo1=mysqli_query($conn, "SELECT Anatomy1 FROM Eval JOIN GreenForm ON Eval.EvalID = GreenForm.EvalID WHERE $StudAndProc");
					$AnatomyRow1=mysqli_fetch_array($AnatomyInfo1);
					echo $AnatomyRow1['Anatomy1']; ?>" class="form-control" disabled>
          </div>

        </div>
        <div class="col-sm-3">
          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">(2)</span>
            </div>
            <input name="bodypart2" value="<?php
	   				$AnatomyInfo2=mysqli_query($conn, "SELECT Anatomy2 FROM Eval JOIN GreenForm ON Eval.EvalID = GreenForm.EvalID WHERE $StudAndProc");
					$AnatomyRow2=mysqli_fetch_array($AnatomyInfo2);
					echo $AnatomyRow2['Anatomy2']; ?>" class="form-control" disabled>
          </div>
        </div>
        <div class="col-sm-3">


          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">(3)</span>
            </div>
            <input name="bodypart3" value="<?php
	   				$AnatomyInfo3=mysqli_query($conn, "SELECT Anatomy3 FROM Eval JOIN GreenForm ON Eval.EvalID = GreenForm.EvalID WHERE $StudAndProc");
					$AnatomyRow3=mysqli_fetch_array($AnatomyInfo3);
					echo $AnatomyRow3['Anatomy3']; ?>" class="form-control" disabled>
          </div>
        </div>
      </div>
      <br>
<!-- FIX THE RESPONSIVENESS ISSUE BITCH -->
      <div class="row">
        <div class="col-sm-2">
          <div>
            <p><strong>Comments:</strong></p>
          </div>
        </div>
        <div class="col-sm-8 ">
          <div class="input-group ">
	  <textarea name="name" rows="2" cols="60" maxlength="255" class="align-center form-control" disabled><?php
		$EvalComments=mysqli_query($conn, "SELECT Eval.Comments FROM Eval JOIN GreenForm ON Eval.EvalID = GreenForm.EvalID WHERE $StudAndProc");
		$EvalCommentsRow=mysqli_fetch_array($EvalComments);
		 echo $EvalCommentsRow['Comments']; ?>
           </textarea>
          </div>
        </div>

      </div>
      <hr>
      <div class="row"> <!-- COMPETENCY EVALUATION HEADING-->
      <div class="col-sm-4 offset-4">
        <h2 class="lead text-center">COMPETENCY VERIFICATION</h2>

      </div>
      </div>


      <div class="row"> <!-- COMPETENCY POLICY NOTES -->
        <div class="col-sm-10 offset-1">
          <div class="text-center">

          <p><strong>A minimum of 73 points (73% = "C" minimum passing) is required for a completion of a competency.
            <br>ARRT requires that failing scores must be recorded and the exam to be repeated for documenting competency.</strong></p>
          </div>
        </div>
      </div>
<hr>
      <div class="row">
        <div class="col-sm-8 offset-1 pt-3">
          <p><strong>Do you feel this student can perform this examination consistently and competently?</strong></p>
        </div>
        <div class="col-sm-2 offset-1 pt-2">
	  <div class="btn btn-group btn-group-toggle btn-group-sm" data-toggle="buttons">
	   <?php
		$ConsistentQues=mysqli_query($conn, "SELECT Consistent FROM GreenForm WHERE $StudAndProc");
		$Consistent=mysqli_fetch_array($ConsistentQues);
		if($Consistent['Consistent'] == 1)
		{
          	   echo '<label class="btn btn-info">
      				<input type="radio" name="options" value="yes" autocomplete="off" disabled> <span style="font-weight: bold;">Yes</span>
          		</label>
          		<label class="btn btn-secondary disabled">
      				<input type="radio" name="options" value="no" autocomplete="off" disabled> <span style="font-weight: bold;">No</span>
			</label>';
		}
		else if($Consistent['Consistent'] == 0)
		{
		  echo '<label class="btn btn-secondary disabled">
      				<input type="radio" name="options" value="yes" autocomplete="off" disabled> <span style="font-weight: bold;">Yes</span>
          		</label>
          		<label class="btn btn-danger">
      				<input type="radio" name="options" value="no" autocomplete="off" disabled> <span style="font-weight: bold;">No</span>
			</label>';
		}
	?>
          </td>
          </div>
        </div>
      </div>

<hr>

      <div class="row">
	<div class="col-sm-8 offset-1 pt-3">
	   <p><strong>Is this a proficiency exam?</strong></p>
	</div>
	<div class="col-sm-2 offset-1">
	  <div class="btn btn-group btn-group-toggle btn-group-sm" data-toggle="buttons">
		<?php
			$ProfQues=mysqli_query($conn, "SELECT ProficiencyExam FROM GreenForm WHERE $StudAndProc");
			$Prof = mysqli_fetch_array($ProfQues);
			if($Prof['ProficiencyExam'] == 1) {
	  			echo '<label class="btn btn-primary">
					<input type="radio" name="proficiency" value="yes" autocomplete="off" disabled><span style="font-weight: bold;">Yes</span>
	  			     </label>
	  			     <label class="btn btn-secondary disabled">
					<input type="radio" name="proficiency" value="no" autocomplete="off" disabled><span style="font-weight: bold;">No</span>
	  			     </label>';
			}
			else if($Prof['ProficiencyExam'] == 0) {
	  			echo '<label class="btn btn-secondary disabled">
					<input type="radio" name="proficiency" class="form-control form-control-sm" value="yes" autocomplete="off" disabled><span style="font-weight: bold;">Yes</span>
	  			     </label>
	  			     <label class="btn btn-danger">
					<input type="radio" name="proficiency" class="form-control form-control-sm" value="no" autocomplete="off" disabled><span style="font-weight: bold;">No</span>
	  			     </label>';
			}
		?>	
	</div>
      </div>
    </div>

<hr>
<div class="row"> <!-- COMPETENCY EVALUATION HEADING-->
  <div class="col-sm-4 offset-4">
    <h2 class="lead text-center">Any Further Comments:</h2>
  </div>
</div>
<br>
  <div class="row">
    <div class="col-sm-8 offset-2">
      <div class="input-group ">
        <textarea name="name" rows="4" cols="82" class="form-control" maxlength="255" disabled><?php
		$GFComments=mysqli_query($conn, "SELECT Comments FROM GreenForm WHERE $StudAndProc");
		$GFCommentsRow=mysqli_fetch_array($GFComments);
		echo $GFCommentsRow['Comments']; ?>
	</textarea>
      </div>
    </div>
  </div>

  <hr>

  <div class="row">
    <div class="col-sm-2 offset-1">
      <p class="lead mt-2">Student Signature:<p/>
    </div>
    <div class="col-sm-6">
    <div id="studSig">
	<img src="<?php
			$getStudSigLocation=mysqli_query($conn, "SELECT StudentSig FROM GreenForm WHERE $StudAndProc");
			$getStudSigLocationRow=mysqli_fetch_array($getStudSigLocation);
			$StudSigLocation = $getStudSigLocationRow['StudentSig']; 
			if($StudSigLocation == '') {
				echo "signatures/default.png";
			}
			else {
				#echo "signatures/TestStudentChestRoutineObservationSig.png";
				echo $StudSigLocation;
			}?>"  />				
    </div>
    </div>
    </div>

<!--    <div class="row">
	<div class="col-sm-2 offset-1">
		<p class="lead mt-2">Score: </p>
	</div>
	<div class="col-sm-4">
		<p class="lead mt-2"><?php echo $Score; ?></p>
	</div> 
    </div>
-->
    <div class="row pb-4">
      <div class="col-sm-6 offset-2">
        <button type="button" class="btn btn-danger" onclick="location.href='http://mciviewbox.ddns.net/studentCompletedForms.php'" name="goback">Go Back</button>

      </div>

    </div>



    </form>



  </div> <!-- END CONTAINER -->
   </body>
 </html>
