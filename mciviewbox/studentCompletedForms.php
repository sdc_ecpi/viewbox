<?php
session_start();

require('dbconn.php');

if(!isset($_SESSION['username'])) {

	header("Location:http://mciviewbox.ddns.net");

}

?>


<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">



</head>

<body>
<?php
$user = $_SESSION['username'];
$getStudentID = mysqli_query($conn, "SELECT StudentID FROM Student WHERE ECPIID = '$user'");
$studentIdRow = mysqli_fetch_array($getStudentID);
$studentID = $studentIdRow['StudentID'];
$ProcNames = mysqli_query($conn, "SELECT ProcedureName FROM Proc WHERE ProcedureID IN (SELECT ProcID FROM GreenForm WHERE StudID = '$studentID')");
$getStudentName = mysqli_query($conn, "SELECT CONCAT(FirstName, ' ', LastName) AS StudentName FROM Student WHERE ECPIID = '$user'");
$StudNameRow = mysqli_fetch_array($getStudentName);
$StudName = $StudNameRow['StudentName'];
?>
<nav class="navbar navbar-light bg-light">
	<a class="navbar-brand" style='font-size: 24px; color: #C70A0D;' href="http://mciviewbox.ddns.net">MCI ViewBox</a>
</nav>
<div class="fluid-container">
<form method='post' action="<?= htmlentities('greenformstudtemp.php')?>">
<div class="jumbotron" style="background-color: #F0FAFF; box-shadow: 0px 0px 25px #BDCCFF;">

<div class="form-group">

<div class="col-sm-6 text-right" style="font-weight:bold; font-size: 18px;">Student Name:</div>
<div class="col-sm-6 text-left" style='font-size:18px; font-weight: bold; color: #30AC59;'>
<?php
	echo $StudName;
?>
</div>
<br>
<br>
<div class="col-sm-6 text-right" style="font-weight:bold; font-size: 18px;">Forms Completed:</div>
<div class="col-sm-6 text-left">
<select id="Selectforms" name="procs">
<option id="checking" value="-1">--SELECT FORM--</option>

<?php

while($ProcRow=mysqli_fetch_array($ProcNames))
	      {
		echo "<option id='checking' value='".$ProcRow['ProcedureName']."'>".$ProcRow['ProcedureName']."</option>";

	      }
	?>


</select>
<br>
</div>
<br>
<br>
<br>
</div>
<div class="text-center">
<button id="submitBtn" type="submit" class="btn btn-primary" style="font-size: 16px;">View Green Form</button>
</div>

</div>

</form>
</div>
<?php
$_SESSION["Student"] = $StudName;



?>






</body>

<script>

$('#Selectforms').change(function() {

	var op = $(this).val();
	
	if (op != '-1') {
	
		$('#submitBtn').prop("disabled", false);
	}
	else {
		
		$('#submitBtn').prop("disabled", true);
	}


});

$(function() {



$('#submitBtn').prop("disabled", true);


});

		
	


	





</script>







</html>
