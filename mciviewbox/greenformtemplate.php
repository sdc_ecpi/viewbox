<!-- TODO:
  fix dropdown on procedure field
  fix submit button alignment
  fix functionality of yes/no in evals
  fix dimensions of print and signature
  add proficiency
 -->




 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title>Green Form Template</title>
     <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
     <style media="screen">
       .table-borderless td, .table-borderless th {
         border: none;
       }

       input {
         btn-group-toggle
       }
     </style>





   </head>
   <body>
	     <!-- style="background-color: #d7eddd"    #61b579-->
     <!-- nav bar with editing commands and buttons -->
    <div class="container container-fluid mt-2 mb-5 shadow-lg" style="background-color: #5ec177"> <!--begin transcribe from green form-->
    <form class="pt-2" method="post" action=<?= htmlentities('../../index.php')?>>
     <div class="form-group">


       <div class="row" id="Univ-header">

         <div class="col-sm-10 offset-sm-1">
           <p class="text-center" style="font-weight: bolder;">MEDICAL CAREERS INSTITUTE COLLEGE OF HEALTH SCIENCE OF ECPI UNIVERSITY MEDICAL RADIOLOGY PROGRAM</p>
           <br>
           <h1 class="lead text-center">CLINICAL COMPETENCY EVALUATION FORM</h1> <!-- BEGIN FIRST SUBHEADING -->

         </div>
         <div class="col-sm-2">

         </div>
         <h4 ></h4>

     </div> <!-- END UNIV HEADER -->
           <br>
      <div class="row" id="form-meta-info"> <!-- FORM META INFO ROW 1 -->
        <div class="col-sm-6 ">
          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">Student Name</span>
	    </div>

           <select name="student" class="custom-select custom-select-sm">
			<option value='Test Student'>Test Student</option>	   </select>

          </div>
        </div>
        <div class="col-sm-6">
          <!-- QUERY DATABASE FOR PROCEDURES -->
          <div class="input-group input-group-sm ">
            <div class="input-group-prepend">
              <span class="input-group-text">Procedure</span>
            </div>

		<select name="procs" id="drop" class="custom-select custom-select-sm">
			<option value='Chest Routine'>Chest Routine</option><option value='Chest AP (Wheelchair or Stretcher'>Chest AP (Wheelchair or Stretcher</option><option value='Ribs'>Ribs</option><option value='Chest Lateral Decubitus'>Chest Lateral Decubitus</option><option value='Sternum'>Sternum</option><option value='Upper Airway (Soft-Tissue Neck'>Upper Airway (Soft-Tissue Neck</option><option value='Thumb or Finger'>Thumb or Finger</option><option value='Hand'>Hand</option><option value='Wrist'>Wrist</option><option value='Forearm'>Forearm</option><option value='Elbow'>Elbow</option><option value='Humerus'>Humerus</option><option value='Shoulder'>Shoulder</option><option value='Trauma: Shoulder or Humerus (Scapular Y, Transthoraric, or Axial)'>Trauma: Shoulder or Humerus (Scapular Y, Transthoraric, or Axial)</option><option value='Clavicle'>Clavicle</option><option value='Scapula'>Scapula</option><option value='AC Joints'>AC Joints</option><option value='Trauma: Upper Extremity(Non Shoulder)'>Trauma: Upper Extremity(Non Shoulder)</option><option value='Toes'>Toes</option><option value='Foot'>Foot</option><option value='Ankle'>Ankle</option><option value='Knee'>Knee</option><option value='Tibia-Fibula'>Tibia-Fibula</option><option value='Femur'>Femur</option><option value='Trauma: Lower Extremity'>Trauma: Lower Extremity</option><option value='Patella'>Patella</option><option value='Calcaneus'>Calcaneus</option><option value='Skull'>Skull</option><option value='Paranasal Sinuses'>Paranasal Sinuses</option><option value='Facial Bones'>Facial Bones</option><option value='Orbits'>Orbits</option><option value='Zygomatic Arches'>Zygomatic Arches</option><option value='Mandible'>Mandible</option><option value='Temporomandibular Joints'>Temporomandibular Joints</option><option value='Cervical Spine'>Cervical Spine</option><option value='Thoracic Spine'>Thoracic Spine</option><option value='Lumbar Spine'>Lumbar Spine</option><option value='Cross-Table (Horizontal Beam) Lateral Spine'>Cross-Table (Horizontal Beam) Lateral Spine</option><option value='Pelvis'>Pelvis</option><option value='Hip'>Hip</option><option value='Cross-Table (Horizontal Beam) Lateral Hip'>Cross-Table (Horizontal Beam) Lateral Hip</option><option value='Sacrum and/or Coccyx'>Sacrum and/or Coccyx</option><option value='Scoliosis Series'>Scoliosis Series</option><option value='Sacroiliac Joints'>Sacroiliac Joints</option><option value='Abdomen Supine (KUB)'>Abdomen Supine (KUB)</option><option value='Abdomen Upright'>Abdomen Upright</option><option value='Abdomen Decubitus'>Abdomen Decubitus</option><option value='Intravenous Urography'>Intravenous Urography</option><option value='Upper GI Series, Single or Double Contrast'>Upper GI Series, Single or Double Contrast</option><option value='Contrast Enema, Single or Double Contrast'>Contrast Enema, Single or Double Contrast</option><option value='Small Bowel Series'>Small Bowel Series</option><option value='Esophagus'>Esophagus</option><option value='Cystopraphy/Cystourethrography'>Cystopraphy/Cystourethrography</option><option value='ERCP'>ERCP</option><option value='Myelography'>Myelography</option><option value='Arthrography'>Arthrography</option><option value='Hysterosalpingography'>Hysterosalpingography</option><option value='C-Arm Procedure (Requiring Manipulation to Obtain More Than One Projection)'>C-Arm Procedure (Requiring Manipulation to Obtain More Than One Projection)</option><option value='Surgical C-Arm Procedure (Requiring Manipulation Around a Sterile Field)'>Surgical C-Arm Procedure (Requiring Manipulation Around a Sterile Field)</option><option value='Mobile Chest'>Mobile Chest</option><option value='Mobile Abdomen'>Mobile Abdomen</option><option value='Mobile Orthopedic'>Mobile Orthopedic</option><option value='Pediatric Chest Routine'>Pediatric Chest Routine</option><option value='Pediatric Upper Extremity'>Pediatric Upper Extremity</option><option value='Pediatric Lower Extremity'>Pediatric Lower Extremity</option><option value='Pediatric Abdomen'>Pediatric Abdomen</option><option value='Pediatric Mobile Study'>Pediatric Mobile Study</option><option value='Geriatric Chest Routine'>Geriatric Chest Routine</option><option value='Geriatric Upper Extremity'>Geriatric Upper Extremity</option><option value='Geriatric Lower Extremity'>Geriatric Lower Extremity</option>		</select>

            <!-- M/E/S? -->
          </div>
        </div>
      </div> <!-- END FORM META INFO ROW 1-->
        <br>
        <div class="row">  <!-- BEGIN FORM META INFO ROW 2 -->
          <div class="col-sm-7">
            <div class="input-group input-group-sm">
              <div class="input-group-prepend">
                <span class="input-group-text">Competency Date</span>
	      </div>

              <input type="date" name="compDate" class="form-control">

            </div>
          </div>

          <div class="col-sm-5">

              <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                  <span class="input-group-text">Clinical Site</span>
                </div>

		<select name="site" class="custom-select custom-select-sm">
			<option value='Sentara Albemarle Hospital Elizabeth City, NC'>Sentara Albemarle Hospital Elizabeth City, NC</option><option value='Childrens Hospital of Kings Daughters (CHKD) Norfolk, VA'>Childrens Hospital of Kings Daughters (CHKD) Norfolk, VA</option><option value='Patient First Cedar Road Chesapeake, VA'>Patient First Cedar Road Chesapeake, VA</option><option value='Now Care Virginia Beach, VA'>Now Care Virginia Beach, VA</option><option value='SMOC (Sports Medicine & Ortho) Chesapeake, VA'>SMOC (Sports Medicine & Ortho) Chesapeake, VA</option><option value='SMOC (Sports Medicine & Ortho) Suffolk, VA'>SMOC (Sports Medicine & Ortho) Suffolk, VA</option><option value='Mary Immaculate Hospital Newport News, VA'>Mary Immaculate Hospital Newport News, VA</option><option value='Bon Secours Rappahannock General Hospital Kilmarnock, VA'>Bon Secours Rappahannock General Hospital Kilmarnock, VA</option><option value='Sentara Williamsburg Region Medical Center Williamsburg, VA'>Sentara Williamsburg Region Medical Center Williamsburg, VA</option><option value='M.D. Express Grafton Yorktown, VA'>M.D. Express Grafton Yorktown, VA</option><option value='M.D. Express Williamsburg, VA'>M.D. Express Williamsburg, VA</option><option value='M.D. Express Newport News, VA'>M.D. Express Newport News, VA</option><option value='M.D. Express Hampton, VA'>M.D. Express Hampton, VA</option><option value='Patient First Denbeigh Newport News, VA'>Patient First Denbeigh Newport News, VA</option><option value='TPMG (Tidewater Physicians Multispecialty Group) Williamsburg, VA'>TPMG (Tidewater Physicians Multispecialty Group) Williamsburg, VA</option><option value='OSC (Orthopaedic and Spine Center) Newport News (Port Warwick), VA'>OSC (Orthopaedic and Spine Center) Newport News (Port Warwick), VA</option>		</select>

            </div>
          </div>

        </div> <!-- END FORM META INFO ROW 2 -->
        <br>
        <h2 class="lead text-center">DOCUMENTATION by R.T (R)</h2> <!-- BEGIN SECOND HEADING -->
        <div class="row"> <!-- BEGIN DOCUMENTATION TABLE -->


          <table class="table table-borderless ">


              <tr class="shadow-sm text-center " style="background-color: #00ad6a;">
                <th></th>
                <th>Date</th>
                <th>Print</th>
                <th>Signature</th>
                <th>Site</th>
              </tr>



              <tr>
                <th scope="row">1: Observation</th>
                <td><input type="date" name="obsDate" class="form-control form-control-sm">
						</td>

                <td><input type="text" class="form-control form-control-sm"></td>
                <td><input type="text" class="form-control form-control-sm"></td>
                <td><select name="obsSite" class="form-control form-control form-control-sm custom-select custom-select-sm">
			<option value='Sentara Albemarle Hospital Elizabeth City, NC'>Sentara Albemarle Hospital Elizabeth City, NC</option><option value='Childrens Hospital of Kings Daughters (CHKD) Norfolk, VA'>Childrens Hospital of Kings Daughters (CHKD) Norfolk, VA</option><option value='Patient First Cedar Road Chesapeake, VA'>Patient First Cedar Road Chesapeake, VA</option><option value='Now Care Virginia Beach, VA'>Now Care Virginia Beach, VA</option><option value='SMOC (Sports Medicine & Ortho) Chesapeake, VA'>SMOC (Sports Medicine & Ortho) Chesapeake, VA</option><option value='SMOC (Sports Medicine & Ortho) Suffolk, VA'>SMOC (Sports Medicine & Ortho) Suffolk, VA</option><option value='Mary Immaculate Hospital Newport News, VA'>Mary Immaculate Hospital Newport News, VA</option><option value='Bon Secours Rappahannock General Hospital Kilmarnock, VA'>Bon Secours Rappahannock General Hospital Kilmarnock, VA</option><option value='Sentara Williamsburg Region Medical Center Williamsburg, VA'>Sentara Williamsburg Region Medical Center Williamsburg, VA</option><option value='M.D. Express Grafton Yorktown, VA'>M.D. Express Grafton Yorktown, VA</option><option value='M.D. Express Williamsburg, VA'>M.D. Express Williamsburg, VA</option><option value='M.D. Express Newport News, VA'>M.D. Express Newport News, VA</option><option value='M.D. Express Hampton, VA'>M.D. Express Hampton, VA</option><option value='Patient First Denbeigh Newport News, VA'>Patient First Denbeigh Newport News, VA</option><option value='TPMG (Tidewater Physicians Multispecialty Group) Williamsburg, VA'>TPMG (Tidewater Physicians Multispecialty Group) Williamsburg, VA</option><option value='OSC (Orthopaedic and Spine Center) Newport News (Port Warwick), VA'>OSC (Orthopaedic and Spine Center) Newport News (Port Warwick), VA</option>			</select>
			</td>
              </tr>
              <tr>
                <th scope="row">2: Assisted</th>
                <td><input type="date" name="assDate" class="form-control form-control-sm">
						</td>
                <td><input type="text" class="form-control form-control-sm"></td>
                <td><input type="text" class="form-control form-control-sm"></td>
                <td><select name="assSite" class="form-control form-control-sm custom-select custom-select-sm">
			<option value='Sentara Albemarle Hospital Elizabeth City, NC'>Sentara Albemarle Hospital Elizabeth City, NC</option><option value='Childrens Hospital of Kings Daughters (CHKD) Norfolk, VA'>Childrens Hospital of Kings Daughters (CHKD) Norfolk, VA</option><option value='Patient First Cedar Road Chesapeake, VA'>Patient First Cedar Road Chesapeake, VA</option><option value='Now Care Virginia Beach, VA'>Now Care Virginia Beach, VA</option><option value='SMOC (Sports Medicine & Ortho) Chesapeake, VA'>SMOC (Sports Medicine & Ortho) Chesapeake, VA</option><option value='SMOC (Sports Medicine & Ortho) Suffolk, VA'>SMOC (Sports Medicine & Ortho) Suffolk, VA</option><option value='Mary Immaculate Hospital Newport News, VA'>Mary Immaculate Hospital Newport News, VA</option><option value='Bon Secours Rappahannock General Hospital Kilmarnock, VA'>Bon Secours Rappahannock General Hospital Kilmarnock, VA</option><option value='Sentara Williamsburg Region Medical Center Williamsburg, VA'>Sentara Williamsburg Region Medical Center Williamsburg, VA</option><option value='M.D. Express Grafton Yorktown, VA'>M.D. Express Grafton Yorktown, VA</option><option value='M.D. Express Williamsburg, VA'>M.D. Express Williamsburg, VA</option><option value='M.D. Express Newport News, VA'>M.D. Express Newport News, VA</option><option value='M.D. Express Hampton, VA'>M.D. Express Hampton, VA</option><option value='Patient First Denbeigh Newport News, VA'>Patient First Denbeigh Newport News, VA</option><option value='TPMG (Tidewater Physicians Multispecialty Group) Williamsburg, VA'>TPMG (Tidewater Physicians Multispecialty Group) Williamsburg, VA</option><option value='OSC (Orthopaedic and Spine Center) Newport News (Port Warwick), VA'>OSC (Orthopaedic and Spine Center) Newport News (Port Warwick), VA</option>			</select>
			</td>
              </tr>

          </table>


        </div> <!-- END DOCUMENTATION TABLE -->
        <hr>

        <!-- STUDENT COMMENT IF REQUIRED-->
        <div class="row"> <!-- EVALUATION CRITERIA HEADING-->
        <div class="col-sm-4 offset-4">
          <h2 class="lead text-center">EVALUATION CRITERIA</h2>

        </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="text-center">

            <p><strong>Competency Requirement:</strong> Perform the Radiologic procedure  appropriately, utilizing these criteria:</p>
            <p><strong>There are 25 category criteria ("Yes" / "No")</strong></p>
            <p>All projections for a given examination must be performed correctly in order to recieve a "Yes" for the category.</p>
          </div>
          </div>
        </div>
      </div> <!-- END FORM GROUP -->
      <div class="row">

      <table class="table table-sm table-borderless table-hover"> <!-- BEGIN EVALUATION TABLE -->
        <tbody>

            <!--
            This section screams out to be generated in php. All of the below will be populated by the database, as these 25 criteria can change depending on the type of evaluation, etc.
            php will be rendering pretty much all of this, but here's the template to use. I'll adjust the sizing and spacing, don't worry about that.
           -->


                <tr scope="row" style="background-color: #00ad6a;" class="">
                  <th scope="row" class="">&nbspA. <span class="text-center" >Evaluation of Procedure Requisistion</span></th>
                  <td></td>
           <tr>
            <td scope="row" class="pl-4 pt-3">1. <span class="">Identifies patient using two patient identifiers</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options1" value="yes1" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options1" value="no1" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">2. <span class="">Verifies requisition is applicable according to anatomical part and history</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options2" value="yes2" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options2" value="no2" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">3. <span class="">Obtains patient's history; inquires about possible pregnancy (if applicable)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options3" value="yes3" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options3" value="no3" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
               <tr scope="row" style="background-color: #00ad6a;" >
                  <th scope="row">&nbspB. Room Preparation</th>
                  <td></td>
           <tr>
            <td scope="row" class="pl-4 pt-3">4. <span class="">Cleans room/table/wall unit; linens, pillow, table pad etc.</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options4" value="yes4" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options4" value="no4" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">5. <span class="">Equipment readiness (Table, tube, girds, etc)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options5" value="yes5" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options5" value="no5" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">6. <span class="">Accesories ready (i.e, Flouroscopy aprons, contrast media, etc.)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options6" value="yes6" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options6" value="no6" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
               <tr scope="row" style="background-color: #00ad6a;" >
                 <th scope="row" class="">&nbspC. Patient Assesment and Management</th>
                 <td></td>
           <tr>
            <td scope="row" class="pl-4 pt-3">7. <span class="">Communicates & assess patient throughout the exam (Respiration, appearance, etc.)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options7" value="yes7" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options7" value="no7" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">8. <span class="">Provides patient assistance & comfort throughout the exam (transfer, positioning, etc)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options8" value="yes8" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options8" value="no8" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">9. <span class="">Makes adjustments for medical Equipment: I.V / 02 / tubing/leads, etc. (if applicable)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options9" value="yes9" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options9" value="no9" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row" class="">&nbspD. Equipment Operation and Technique</th>
                 <td></td>
           <tr>
            <td scope="row" class="pl-4 pt-3">10. <span class="">Correct tube and Table Handling (SID, Locks, Detent, Movements & Angles)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options10" value="yes10" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options10" value="no10" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">11. <span class="">Correct selection of Technical Factors: </span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options11" value="yes11" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options11" value="no11" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">12. <span class="">Correct Receptor Selection (Table top/bucky/IR alignment/ Size / Placement)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options12" value="yes12" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options12" value="no12" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspE. Positioning Skills</th>
                <td></td>
           <tr>
            <td scope="row" class="pl-4 pt-3">13. <span class="">Performs the correct Projection/Position(s) for Anatomical Part (AP/PA/Lateral, etc.)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options13" value="yes13" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options13" value="no13" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">14. <span class="">Correct Body Part alignment (Erect/Supine/Prone/Oblique/Lateral)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options14" value="yes14" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options14" value="no14" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">15. <span class="">Correct central ray placement</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options15" value="yes15" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options15" value="no15" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">16. <span class="">Correct use of Anatomical Markers and Positioning Aids</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options16" value="yes16" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options16" value="no16" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspF. Radiation Safety</th>
                 <td></td>
           <tr>
            <td scope="row" class="pl-4 pt-3">17. <span class="">Provides proper Protection (Sheilding / Collimation / Technique)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options17" value="yes17" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options17" value="no17" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">18. <span class="">Ensure Self and Others are Protected (sheilding / barrier / Distance / Exposure Announcement)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options18" value="yes18" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options18" value="no18" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspG. Image Aquisition</th>
                 <td></td>
           <tr>
            <td scope="row" class="pl-4 pt-3">19. <span class="">Correctly inputs & verifies patient demographic information on workstation/ PACS</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options19" value="yes19" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options19" value="no19" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">20. <span class="">Correctly exposes and processes the image (processor/computer)</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options20" value="yes20" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options20" value="no20" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspH. Image analysis and Examination Completion</th>
                 <td></td>
           <tr>
            <td scope="row" class="pl-4 pt-3">21. <span class="">Images demonstrate accurate positioning & demonstrate all required anatomy</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options21" value="yes21" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options21" value="no21" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">22. <span class="">Images demonstrate acceptable density, contrast and collination</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options22" value="yes22" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options22" value="no22" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">23. <span class="">Images are free of artifacts, distortion, and motion, etc.</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options23" value="yes23" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options23" value="no23" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
           <tr>
            <td scope="row" class="pl-4 pt-3">24. <span class="">Completes exam in a timely manner & patient given follow-up instructions</span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options24" value="yes24" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options24" value="no24" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>
               <tr scope="row" style="background-color: #00ad6a;" class="">
                 <th scope="row">&nbspI. Anatomical Parts Identification</th>
                 <td></td>
           <tr>
            <td scope="row" class="pl-4 pt-3">25. <span class="">Correctly identifies at least three(3) anatomical structures <em>(no partial credit allowed)</em></span></td>
            <td>
            <div class=" btn-sm btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
	      <input type="radio" name="options25" value="yes25" autocomplete="off" ><span style=" font-weight: bold;">Yes</span>
            </label>
            <label class="btn btn-secondary">
	      <input type="radio" name="options25" value="no25" autocomplete="off"> <span style=" font-weight: bold;">No</span>
            </label>

            </div>
            </td>
          </tr>

         </tr>
        </tbody>
      </table>
    </div> <!-- END EVALUATION SECTION -->
      <hr>
      <div class="row">
        <div class="col-sm-10 offset-1">

          <p class="text-center">Note: R.T (R) may have the student identify specific anatomy or ask the student to point to requested anatomy</p>
        </div>

      </div>
      <div class="row">
        <div class="col-sm-2">
          <div>
            <p><strong>List Anatomy:</strong></p>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">(1)</span>
            </div>
            <input type="text" name="bodypart1" value="" class="form-control">
          </div>

        </div>
        <div class="col-sm-3">
          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">(2)</span>
            </div>
            <input type="text" name="bodypart2" value="" class="form-control">
          </div>
        </div>
        <div class="col-sm-3">


          <div class="input-group input-group-sm">
            <div class="input-group-prepend">
              <span class="input-group-text">(3)</span>
            </div>
            <input type="text" name="bodypart3" value="" class="form-control">
          </div>
        </div>
      </div>
      <br>
<!-- FIX THE RESPONSIVENESS ISSUE BITCH -->
      <div class="row">
        <div class="col-sm-2">
          <div>
            <p><strong>Comments:</strong></p>
          </div>
        </div>
        <div class="col-sm-8 ">
          <div class="input-group ">
            <textarea name="name" rows="2" cols="60" maxlength="255" class="align-center form-control">
              0123456789101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899100101102103104105106107108109110111112113114115116117118119120121            </textarea>
          </div>
        </div>

      </div>
      <hr>
      <div class="row"> <!-- COMPETENCY EVALUATION HEADING-->
      <div class="col-sm-4 offset-4">
        <h2 class="lead text-center">COMPETENCY EVALUTION</h2>

      </div>
      </div>


      <div class="row"> <!-- COMPETENCY POLICY NOTES -->
        <div class="col-sm-10 offset-1">
          <div class="text-center">

          <p><strong>A minimum of 73 points (73% = "C" minimum passing) is required for a completion of a competency.
            <br>ARRT requires that failing scores must be recorded and the exam to be repeated for documenting competency.</strong></p>
          </div>
        </div>
      </div>
<hr>
      <div class="row">
        <div class="col-sm-8 offset-1 pt-3">
          <p><strong>Do you feel this student can perform this examination consistently and <a href="https://www.google.com">google</a> competently?</strong></p>
        </div>
        <div class="col-sm-2 offset-1">
          <div class="btn btn-group btn-group-toggle" data-toggle="buttons">
          <label class="btn btn-secondary">
      <input type="radio" name="options" value="yes" autocomplete="off"> <span style="font-weight: bold;">Yes</span>
          </label>
          <label class="btn btn-secondary">
      <input type="radio" name="options" value="no" autocomplete="off"> <span style="font-weight: bold;">No</span>
          </label>
          </td>
          </div>
        </div>
      </div>

<hr>

<div class="row"> <!-- COMPETENCY EVALUATION HEADING-->
  <div class="col-sm-4 offset-4">
    <h2 class="lead text-center">Any Further Comments:</h2>
  </div>
</div>
<br>
  <div class="row">
    <div class="col-sm-8 offset-2">
      <div class="input-group ">
        <textarea name="name" rows="4" cols="82" class="form-control" maxlength="255"></textarea>
      </div>
    </div>
  </div>

  <br>

    <div class="row pb-4">
      <div class="col-sm-6 offset-2">
        <button type="button" class="btn btn-danger" name="goback">Go Back</button>
      </div>

      <div class="col-sm-2 offset-1">
        <button type="button btn-lg" class="btn btn-primary" id="submitBtn" name="submit" class="float-right">Submit</button>

      </div>
    </div>



    </form>



  </div> <!-- END CONTAINER -->
  Doesn't ExistEvaluation Total: 0   </body>
 </html>
