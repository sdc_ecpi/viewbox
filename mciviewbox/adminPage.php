<?php
#	session_start();
#	if(!isset($_SESSION['username'])) {
#		header("Location: http://mciviewbox.ddns.net");
#		die();
#	}


?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <title>Admin Page</title>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.css" />
    <style media="screen">
      .panel {
        font-weight: bold;
      }
      .pre-scrollable {
        max-height: 700px;
        overflow-y: scroll;
      }
    </style>

  </head>
  <body class="bg-dark">


  <div class="container-fluid">

    <div class="row p-2 h-100">
      <div class="col-sm-2" > <!-- begin control column-->
        <div class="col-sm-12"> <!-- this creates a nice margin between the two rows.-->


        <div class="row "> <!-- begin control panel row -->
          <div class="col-sm-12 rounded p-1 pt-2 pb-2" style="background-color: #e9ecef">

            <div class="row mb-1">
              <div class="col-sm-8  ">

                <p class=" text-center m-0 lead" >Overview:</p>
              </div>

              <div class="col-xs-4">
                <button type="button" class="btn bg-light  oi oi-layers " onclick="controlPanelHandler(this.id)" id="over"></button>

              </div>
              <!-- <div class="col-sm-2 ml-2 rounded bg-light p-1 p2-2" id="over" onclick="controlPanelHandler(this.id)">

                  <span class="oi oi-layers m-0"></span>


              </div> -->
            </div>

            <div class="row p-1">
              <div class="col-sm-12">
                <button type="button" class="btn btn-outline-info btn-block panel" onclick="controlPanelHandler(this.id)" id="view">View Form</button>
              </div>
            </div>
            <div class="row p-1">
              <div class="col-sm-12">
                <button type="button " class="btn btn-outline-primary btn-block panel" onclick="controlPanelHandler(this.id)" id="modF">Modify Form</button>
              </div>
            </div>

            <div class="row p-1">
              <div class="col-sm-12 ">
                <button type="button" class="btn btn-outline-success btn-block panel" onclick="controlPanelHandler(this.id)" id="make">New User</button>
              </div>
            </div>

            <div class="row p-1">
              <div class="col-sm-12">
                <button type="button" class="btn btn-outline-warning btn-block panel" onclick="controlPanelHandler(this.id)" id="modU">Modify User</button>
              </div>
            </div>

            <div class="row p-1">
              <div class="col-sm-12">
                <button type="button" class="btn btn-outline-danger btn-block panel" onclick="controlPanelHandler(this.id)" id="del">Delete User</button>
              </div>
            </div>

        </div> <!-- end control panel column-->
      </div> <!-- end contrl panel row-->


      <div class="row mt-2 "> <!-- begin unknown panel maybe a search bar? -->
        <div class="col-sm-12 rounded " style="background-color: #e9ecef;">
          <div class="row">
            <div class="col-sm-12 m-0" >
              <p class="text-center m-0 lead">Active Students:</p>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-12" id="studentList">
              <ul class="list-group pre-scrollable pb-1">
                <?php
			require('dbconn.php');

			$getStudent = mysqli_query($conn, "SELECT CONCAT(FirstName, ' ', LastName) AS FullName FROM Student");
		while($StudentArray = mysqli_fetch_array($getStudent)) {
                   echo "<li class='list-group-item py-1'>" . $StudentArray['FullName'] . "</li>";
		}
                ?>

                <!-- Zach needs to add an action to the list items that will take the admin to a page with all the student's completed evals-->
              </ul>
            </div>
          </div>



        </div>

      </div> <!-- end unknown panel maybe a search bar?-->

    </div> <!-- end nice marginifier -->
  </div> <!-- end control column -->
      <div class="col-sm-10"> <!-- BEGIN ACTIVE PANE CONTAINER -->
        <!-- Here, I can use JS or vue to control what gets shown and what doesn't depending on the buttons pushed.  -->

        <!-- maybe use ajax here to load a url? -->
        <iframe src="controlFiles/overviewFiles/overview.php" width="100%" height="130%" style="border: none;" class="rounded" id="frame"></iframe> <!-- fix this, the url isn't updating-->



      </div> <!-- END ACTIVE PANE CONTAINER -->
    </div>
  </div>
  </body>
</html>


<script type="text/javascript">
//this is the router for each button. this sets the path for each button for the iframe to go to.
  const pageMap = {
    "modU": "modifyUserFiles/modifyUser.php",
    "del": "deleteUserFiles/deleteUser.php",
    "make": "createUserFiles/createUser.php",
    "modF": "modifyFormFiles/modifyForm.php",
    "view": "viewFormFiles/viewForm.php",
    "over": "overviewFiles/overview.php",
    "frame": "overviewFiles/overview.php", //initial loading path

  }
  let frame = document.getElementById('frame');
  let controlPanelHandler = ((button) => {
    console.log("clicked: " + button);
    frame.src = "controlFiles/" + pageMap[button];
  });
</script>
