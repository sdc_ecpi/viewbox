<?php
session_start();


require('../dbconn.php');

$username = $_SESSION['username'];



$getStudentUser = mysqli_query($conn, "SELECT ECPIID FROM Student WHERE ECPIID = '$username'");

$getAdminOrClinicianUser = mysqli_query($conn, "SELECT UserID FROM Users WHERE UserID = '$username'");

$getAdminOrClinicianInfo = mysqli_fetch_array($getAdminOrClinicianUser);
$getAdminRow = $getAdminOrClinicianInfo['Admin'];
$getClinicianRow = $getAdminOrClinicianInfo['Clinician'];


if (mysqli_num_rows($getStudentUser) == 0 && $getAdminRow == 0 && $getClinicianRow == 0) {
	echo "<p style='text-align:center; color: FF9F14; font-size: 32px; text-shadow: 1px 1px 1px black;'>Username/Email does not exist!</p>";
	echo '<div style="text-align:center;"><button onclick="location.href=\'http://mciviewbox.ddns.net/resetPassword.php\'">Go Back</button></div>';
	exit;
}


if($getAdminRow == 1) {
	$userType = "admin";
}
else if($getClinicianRow == 1) {
	$userType = "clinician";
}
else {
	$userType = "student";
}



?>


<!DOCTYPE html>
<html>
	<head>
		<title>Reset Password</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>


</head>

<script>
	$(document).ready( function() {
	$("#btnSubmit").attr("disabled", true);
	$('#password, #passwordVerify').on('keyup', function() {

		if($('#password').val() == $('#passwordVerify').val() && $('#password').val() != '' && $('passwordVerify') != '')  {
		$('#message').html('Password\'s Matching').css('color', 'green');
		$("#btnSubmit").attr("disabled", false);	
		} else
			{
				$('#message').html('Password\'s Not Matching').css('color', 'red');
				$("#btnSubmit").attr("disabled", true);

			}
});	

});

</script>


	<body>

		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" style="color: #C70A0D" href="http://mciviewbox.ddns.net/">MCI ViewBox</a>

		</nav>
		<!-- END NAV BAR -->

		<!-- BEGIN LOGIN FORM -->
		<div class="jumbotron">

			<div class="row">
				<div class="col-3">

				</div>


				<div class="col-6 ">
					<h4 class="text-center">Reset Password</h4>
					<form method="POST" action="<?= htmlentities('resetPasswordVerify.php?userType='.$userType.'');?>">
						<div class="form group">


							<div class="input-group shadow">
								<div class="input-group-prepend">
									<span class="input-group-text">New Password</span>
								</div>
									<input type="password" id="password" class="form-control" name="password">
							</div>
							<br>
							<div class="input-group  shadow">
								<div class="input-group-prepend">
									<span class="input-group-text">Confirm Password</span>
								</div>
									<input type="password" id="passwordVerify" class="form-control" name="verifyPassword">

							</div>
							<button type="submit" id="btnSubmit" class="btn btn-primary mt-3 float-sm-right">Reset Password</button>
							<br>

	
							<div id="message"></div>

						</div>
					</form>

				</div> <!-- END LOGIN BARS -->


				<div class="col-3">

				</div>


			</div> <!-- END INPUT ROW -->
		</div> <!-- END LOGIN WINDOW -->
	</body>
</html>




