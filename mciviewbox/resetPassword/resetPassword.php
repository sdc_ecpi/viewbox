<?php
require('../dbconn.php');

session_start();
$user = $_POST['username'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Reset Password</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" intergrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>


<div class="jumbotron">


	<div class="container-fluid">
		<form method="post" id="formControl" action="<?= htmlentities('resetPasswordEmail.php');?>">
			<br />
			<h1 class="text-center text-danger" style="font-size: 4rem;">Reset Password</h1>

			<div class="form-group">


			<div class="row">

					<div class="col-sm-4">
						<div class="input-group ">
							<div class="input-group-prepend">
								<span class="input-group-text">Username</span>
							</div>
							<input class="form-control" name='username'/>
						</div>
					</div>


					<div class="col-sm-4">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Email</span>
							</div>
							<input name='email' type='email' id='email' class="form-control"/>
						</div>
					</div>


					<div class="col-sm-4">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Verify Email</span>
							</div>
							<input type='email' name="emailVerify" id='emailVerify' onChange="checkEmail();" class="form-control"/>
						</div>
					</div>
				</div>

			<div class="row">
				<div class="col-sm-12 p-2">
					<div  style='text-align:center;' id="emailCheck"></div>
				</div>
				<br />
			</div>
			<div class="row">

				<div class="col-md-12 text-center">
					<button type="submit" id="btnDisable" class="btn btn-primary btn-lg">Reset Password</button>
				</div>
			</div>
		</div>

			<script>
			$(document).ready(function() {
				console.log("Ready...");
				$("#btnDisable").prop("disabled", true);
			});
			function checkEmail() {
				let email = $("#email").val();
				let emailVerify = $("#emailVerify").val();
				// monkeywrench

				if (email != emailVerify && !isValidEmailAddress(email) && !isValidEmailAddress(emailVerify)) {
					$("#emailCheck").html("<div class='alert alert-danger'><strong style='color:red;'>Emails Do Not Match!</strong></div>");
					$("$btnDisable").prop("disabled", true);

				}
				else if(email != '' && emailVerify != '' && isValidEmailAddress(email) && isValidEmailAddress(emailVerify) && email == emailVerify){
					$("#emailCheck").html("<div class='alert alert-success'><strong style='color:green;'>Emails Match!</strong></div>");
					$("#btnDisable").prop("disabled", false);

				}
			}


			$(document).ready(function () {
				$("#email, #emailVerify").keyup(checkEmail)});


				function isValidEmailAddress(emailAddress) {
					var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
					return pattern.test(emailAddress);

				}

				</script>


			</form>



		</div> <!-- end container -->
	</div> <!-- end jumbotron -->







	</html>
