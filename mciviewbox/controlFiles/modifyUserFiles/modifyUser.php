<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.css" />

    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-warning justify-content-center py-0">

      <a class="navbar-brand"><em>Modifying User</em></a> <!-- make this text light -->

    </nav>

    <div class="jumbotron jumbotron-fluid shadow">
  <div class="container">
    <h1 class="text-center">please enter username:</h1>
    <form class="" action="createStudent.php" method="post">

      <div class="container p-2 rounded shadow">
        <div class="row ">
          <div class="col-sm-4 offset-2">
            <select class="custom-select">
              <option value="student" selected>Student</option>
              <option value="clinician">Clinician</option>
              <option value="admin">Admin</option>
            </select>
          </div>
          <div class="col-sm-6 ">
            <button type="submit" class="btn btn-primary btn-block">Continue</button>
            <!-- add action to go to new page -->
          </div>
        </div>

      </div>
      <div class="row">
        <div class="col-sm-10 offset-2">
          <div class="alert alert-info" role="alert">
    <strong>Note:</strong> Once created, user permissions cannot be modified. (Students cannot be granted administrator permissions).
    To change a person's permissions, delete the old account and then create a new User with desired settings.
</div>

      </div>
      </div>
    </form>
  </div>
</div>




  </body>
</html>




<?php
# mysql login info
#require('dbconn.php');

#getting the username that was filled out
#username=$_POST['username'];

#getting the password that was entered and verifing it.
#$pwd = password_hash($_POST['password'], PASSWORD_DEFAULT);

# sql command to check password
#$sql = "INSERT INTO $tab_name(UserID, Password, Admin, Clinician) VALUES('$username', '$pwd', 1, 0)";
#$sql = "UPDATE Student SET Password = '$pwd', Email = 'bremur5028@students.ecpi.edu' WHERE StudentID = 1";

#checking if query can run or not
// if(!mysqli_query($conn, $sql)) {
	// echo ("Error: " . mysqli_error($conn));
//}
// echo "<br>";
// echo "Hey, " . $username;
// echo "<br>";





#closes connection
// mysqli_close($conn);




# DEBUG FOR ERRORS
#ini_set('display_errors', 1);




?>
