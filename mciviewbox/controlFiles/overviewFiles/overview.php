<!-- queries:
pass rate of all students
percentage
-->
<?php
require('../../dbconn.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.css" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">


  </script>
  <meta charset="utf-8">
  <title>Overview</title>
  <style media="screen">
  .pre-scrollable {
    max-height: 800px;
    overflow-y: scroll;
  }
  .alert-purp {
    background-color: #d9bcff;
  }
  </style>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark justify-content-center py-0" style="background-color: #99ffd1">

    <a class="navbar-brand"><em>Overview</em></a>

  </nav>
  <div class="container-fluid">
    <!-- <div class="row">
    <div class="col-sm-10 offset-1">
    <h1 class="text-center lead">Data</h1>
  </div>
</div> -->
<div class="row">
  <div class="col-sm-12">

    <div id="accordion" role="tablist" class="pre-scrollable">
      <div class="card">
        <div class="card-header" role="tab" id="headingOne">
          <h5 class="mb-0 text-center">
            <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="text-decoration: none; color: black">
              Accredidation Metrics
            </a>
          </h5>
        </div>

        <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
          <div class="card-body">
            <div class="container-fluid">
              <div class="row"> <!--begin graph row -->
                <div class="col-sm-3">
                  <div class="card shadow">
                    <div class="card-header text-center alert-success">
                      <span class="oi oi-document"></span>
                      <span class="card-title text-center">Chest Routine</span>
                    </div>
                    <div class="card-body" data-toggle="modal" data-target="#evalsCompletedModalri">
                      <!-- Button trigger modal -->

                      <!-- Modal -->
                      <div class="modal fade" id="evalsCompletedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModal3Label">Modal title</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              ...
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <p class="card-text">
                        pass rate for chest routine procedure
                      </p>

                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="card">
                    <div class="card-header text-center alert-primary">
                      <span class="oi oi-person"></span>
                      <span class="card-title text-center">Student Pass Rate</span>
                    </div>
                    <div class="card-body">
                        <?php
                        $NumberOfPasses = mysqli_query($conn, "SELECT COUNT(Pass) AS Passes FROM GreenForm WHERE Pass = 1");
                        $NumberOfForms = mysqli_query($conn, "SELECT COUNT(Pass) AS Forms FROM GreenForm WHERE Pass IS NOT NULL");

                        $ActualNumOfPasses = mysqli_fetch_array($NumberOfPasses);
                        $ActualNumOfForm = mysqli_fetch_array($NumberOfForms);

                        $AverageNumberOfPasses = $ActualNumOfPasses['Passes'] / $ActualNumOfForm['Forms'] * 100;

                        echo '<script> var avgNumofPasses = +' . $AverageNumberOfPasses . '
                                       avgNumofPasses = avgNumofPasses.toFixed(2)</script>';
                        ?>

                        <svg width="100%" height="100%">
                          <text x="0" y="40" id="avgPassRateSVG" font-size="1.5rem"></text>
                        </svg>
                        <script type="text/javascript">


                          document.getElementById('avgPassRateSVG').innerHTML=avgNumofPasses + '%'
                        </script>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="card">
                    <div class="card-header text-center alert-info">
                      <span class="oi oi-people"></span>
                      <span class="card-title text-center ">Cohort Pass Rate</span>
                    </div>
                    <div class="card-body">
                      <p class="card-text">
                        average passing rate of all students in a given cohort (average of Student Pass Rate by cohort)
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">

                  <div class="card">
                    <div class="card-header text-center alert-danger">
                      <span class="oi oi-circle-x pr-1"></span>
                      <span class="card-title text-center "> Failure per Eval</span>
                    </div>
                    <div class="card-body">
                      <p class="card-text">
                        See which evaluations students are currently struggling with the most
                      </p>
                    </div>
                  </div>
                </div>

              </div> <!-- end row -->
            </div> <!-- end container -->
          </div>
        </div>
      </div> <!-- end first card -->

      <div class="card">
        <div class="card-header" role="tab" id="headingTwo">
          <h5 class="mb-0 text-center">
            <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="text-decoration: none; color: black">
              Evaluation Info
            </a>
          </h5>
        </div>
        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
          <div class="card-body">
            <div class="container-fluid">
              <div class="row"> <!--begin second graph row -->
                <div class="col-sm-3">
                  <div class="card">
                    <div class="card-header alert-primary">
                      <span class="oi oi-project pr-1"></span>
                      <span class="card-title text-center">Evals Completed</span>
                    </div>
                    <div class="card-body">
                      <p class="card-text">
                        Track Student's progress by completed evaluations
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="card">
                    <div class="card-header alert-warning">
                      <span class="oi oi-timer"></span>
                      <span class="card-title text-center">Track hours logged</span>
                    </div>
                    <div class="card-body">
                      <p class="card-text">
                        Monitor a student's timesheet
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="card">
                    <div class="card-header alert-secondary">
                      <span class="oi oi-people"></span>
                      <span class="card-title text-center">Cohort Pass Rate</span>
                    </div>
                    <div class="card-body">
                      <p class="card-text">
                        average passing rate of all students in a given cohort (average of Student Pass Rate by cohort)
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">

                  <div class="card">
                    <div class="card-header alert-purp">
                      <span class="oi oi-circle-x"></span>
                      <span class="card-title text-center">Failure rate per Evaluation</span>
                    </div>
                    <div class="card-body">
                      <p class="card-text">
                        See which evaluations students are currently struggling with the most
                      </p>
                    </div>
                  </div>
                </div>

              </div> <!-- end row -->
            </div> <!-- end container -->          </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" role="tab" id="headingThree">
            <h5 class="mb-0 text-center">
              <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="text-decoration: none; color: black">
                Cohort Status
              </a>
            </h5>
          </div>
          <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="card-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-sm-4">

                    <div class="card">
                      <div class="card-header text-center">
                        <span class="oi oi-people"></span>
                        <span class="card-title text-center">Cohort 24</span>
                      </div>
                      <div class="card-body">
                        <p class="card-text">
                          average passing rate of all students in a given cohort (average of Student Pass Rate by cohort)
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">

                    <div class="card">
                      <div class="card-header text-center">
                        <span class="oi oi-people"></span>
                        <span class="card-title text-center">Cohort 25</span>
                      </div>
                      <div class="card-body">
                        <p class="card-text">
                          average passing rate of all students in a given cohort (average of Student Pass Rate by cohort)
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">

                    <div class="card">
                      <div class="card-header text-center">
                        <span class="oi oi-people"></span>
                        <span class="card-title text-center">Cohort 26</span>
                      </div>
                      <div class="card-body">
                        <p class="card-text">
                          average passing rate of all students in a given cohort (average of Student Pass Rate by cohort)
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div> <!-- end container -->          </div>
            </div>
          </div>
        </div> <!-- end accordion -->
      </div> <!-- end column -->
    </div> <!-- end row -->
    <hr>



    <div class="row">
      <div class="col-sm-2 offset-5">
        <h1 class="lead text-center">Recent Activity</h1>
      </div>
      <div class="col-sm-1">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-10 offset-1">
        <table class="table table-sm">
          <thead>
            <tr>
              <th>
                <span class="oi oi-timer"></span>
              </th>
              <th>User</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
            </tr>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
            </tr>

          </tbody>
        </table>
      </div>
    </div>

    <hr>
    <br>


  </div> <!-- end container -->







</body>
</html>
