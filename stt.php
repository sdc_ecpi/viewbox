<!DOCTYPE html>
<?php
	#session_start();
	require 'dbconn.php';
?>
<html>
	<head>
		<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
	</head>
<body>
<form id="sampleForm" method="post" action="signtemptest.php">
   <select name='student'>
	<?php
		$StudNames=mysqli_query($conn, "SELECT CONCAT(FirstName, ' ', LastName) AS Name FROM Student");
		while($studRow=mysqli_fetch_array($StudNames)) {
			echo "<option value='".$studRow['Name']."'>".$studRow['Name']."</option>";
		}
	?>
   </select>

   <br />
   <hr>
   <br />

   <select name="proc">
	<?php
		$ProcNames=mysqli_query($conn, "SELECT ProcedureName FROM Proc");
		while($procRow=mysqli_fetch_array($ProcNames)) {
			echo "<option value='".$procRow['ProcedureName']."'>".$procRow['ProcedureName']."</option>";
		}
	?>
   </select>

   <br />
   <hr>
   <br />

   <div id="wrapper1">
   <canvas class="signaturePad rounded" width="400" height="80" style="background-color: grey"></canvas>
      <button type="button" class="btn btn-secondary" onclick="signaturePadObs.clear()">Clear Observation</button>
	<input type="hidden" id="obsdatainfo" name="obsData" value="" />
	<input type="hidden" id="obs" name="obs" value="" />
   </div>

   <br />
   <hr>
   <br />
   
   <div id="wrapper2">
   <canvas id="canvasAssist"  class="signaturePad rounded" width="400" height="80" style="background-color: grey"></canvas>
      <button type="button" class="btn btn-secondary" onclick="signaturePadAssist.clear()">Clear Assist</button>
	<input type="hidden" id="assistdatainfo" name="assistData" value="" />
	<input type="hidden" id="assist" name="assist" value="" />
   </div>

   <br />
   <hr>
   <br />

   <div id="wrapper3">
   <canvas id="canvasStud"  class="signaturePad rounded" width="400" height="80" style="background-color: grey"></canvas>
      <button type="button" class="btn btn-secondary" onclick="signaturePadStud.clear()">Clear Student</button>
	<input type="hidden" id="studdatainfo" name="studData" value="" />
	<input type="hidden" id="stud" name="stud" value="" />
   </div>

   <br />
   <hr>
   <br />

   <button id="save" width="100px" height="35px" style="text-align: center">Save</button>

    <script>
    	// signaturepad documentation: https://github.com/szimek/signature_pad
      	var wrapper1 = document.getElementById('wrapper1'), 
	    obsCanvas = wrapper1.querySelector('canvas'),
	    signaturePadObs;

	var wrapper2 = document.getElementById('wrapper2'),
      	    assistCanvas = wrapper2.querySelector('canvas'),
	    signaturePadAssist;
	
	var wrapper3 = document.getElementById('wrapper3'),
	    studCanvas = wrapper3.querySelector('canvas'),
	    signaturePadStud;
      	
	var signaturePadObs = new SignaturePad(obsCanvas);
	var signaturePadAssist = new SignaturePad(assistCanvas);
	var signaturePadStud = new SignaturePad(studCanvas);

	document.getElementById('save').addEventListener('click', function() {
		if(signaturePadObs.isEmpty() && signaturePadAssist.isEmpty() && signaturePadStud.isEmpty()) {
			return alert("Please provide a signature");
		}
		if(signaturePadAssist.isEmpty() && signaturePadStud.isEmpty()) {
			var data = signaturePadObs.toDataURL('image/png');
			document.getElementById("obsdatainfo").value = data;
			document.getElementById("obs").value = "obs";
			document.forms["sampleForm"].submit();
		}
		else if(signaturePadObs.isEmpty() && signaturePadStud.isEmpty()) {
			var data = signaturePadAssist.toDataURL('image/png');
			document.getElementById("assistdatainfo").value = data;
			document.getElementById("assist").value = "assist";
			document.forms["sampleForm"].submit();
		}
		else if(signaturePadObs.isEmpty() && signaturePadAssist.isEmpty()) {
			var data = signaturePadStud.toDataURL('image/png');
			document.getElementById("studdatainfo").value = data;
			document.getElementById("stud").value = "stud";
			document.forms["sampleForm"].submit();
		}

	});
    </script>

</body>
</html>
	
