-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: Test
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `Proc`
--

LOCK TABLES `Proc` WRITE;
/*!40000 ALTER TABLE `Proc` DISABLE KEYS */;
INSERT INTO `Proc` VALUES (1,'Chest Routine','M'),(2,'Chest AP (Wheelchair or Stretcher)','M'),(3,'Ribs','M'),(4,'Chest Lateral Decubitus','E'),(5,'Sternum','E'),(6,'Upper Airway (Soft-Tissue Neck)','E'),(7,'Thumb or Finger','M'),(8,'Hand','M'),(9,'Wrist','M'),(10,'Forearm','M'),(11,'Elbow','M'),(12,'Humerus','M'),(13,'Shoulder','M'),(14,'Trauma: Shoulder or Humerus (Scapular Y, Transthoraric, or Axial)','M'),(15,'Clavicle','M'),(16,'Scapula','E'),(17,'AC Joints','E'),(18,'Trauma: Upper Extremity (Non Shoulder)','M'),(19,'Toes','E'),(20,'Foot','M'),(21,'Ankle','M'),(22,'Knee','M'),(23,'Tibia-Fibula','M'),(24,'Femur','M'),(25,'Trauma: Lower Extremity','M'),(26,'Patella','E'),(27,'Calcaneus','E'),(28,'Skull','E'),(29,'Paranasal Sinuses','E'),(30,'Facial Bones','E'),(31,'Orbits','E'),(32,'Zygomatic Arches','E'),(33,'Mandible','E'),(34,'Temporomandibular Joints','E'),(35,'Cervical Spine','M'),(36,'Thoracic Spine','M'),(37,'Lumbar Spine','M'),(38,'Cross-Table (Horizontal Beam) Lateral Spine','M'),(39,'Pelvis','M'),(40,'Hip','M'),(41,'Cross-Table (Horizontal Beam) Lateral Hip','M'),(42,'Sacrum and/or Coccyx','E'),(43,'Scoliosis Series','E'),(44,'Sacroiliac Joints','E'),(45,'Abdomen Supine (KUB)','M'),(46,'Abdomen Upright','M'),(47,'Abdomen Decubitus','E'),(48,'Intravenous Urography','E'),(49,'Upper GI Series, Single or Double Contrast','E'),(50,'Contrast Enema, Single or Double Contrast','E'),(51,'Small Bowel Series','E'),(52,'Esophagus','E'),(53,'Cystopraphy/Cystourethrography','E'),(54,'ERCP','E'),(55,'Myelography','E'),(56,'Arthrography','E'),(57,'Hysterosalpingography','E'),(58,'C-Arm Procedure (Requiring Manipulation to Obtain More Than One Projection)','M'),(59,'Surgical C-Arm Procedure (Requiring Manipulation Around a Sterile Field)','M'),(60,'Mobile Chest','M'),(61,'Mobile Abdomen','M'),(62,'Mobile Orthopedic','M'),(63,'Pediatric Chest Routine','M'),(64,'Pediatric Upper Extremity','E'),(65,'Pediatric Lower Extremity','E'),(66,'Pediatric Abdomen','E'),(67,'Pediatric Mobile Study','E'),(68,'Geriatric Chest Routine','M'),(69,'Geriatric Upper Extremity','M'),(70,'Geriatric Lower Extremity','M');
/*!40000 ALTER TABLE `Proc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ClinicalSite`
--

LOCK TABLES `ClinicalSite` WRITE;
/*!40000 ALTER TABLE `ClinicalSite` DISABLE KEYS */;
INSERT INTO `ClinicalSite` VALUES (1,'Sentara Albemarle Hospital','Elizabeth City','NC'),(2,'Childrens Hospital of Kings Daughters (CHKD)','Norfolk','VA'),(3,'Patient First Cedar Road','Chesapeake','VA'),(4,'Now Care','Virginia Beach','VA'),(5,'SMOC (Sports Medicine & Ortho)','Chesapeake','VA'),(6,'SMOC (Sports Medicine & Ortho)','Suffolk','VA'),(7,'Mary Immaculate Hospital','Newport News','VA'),(8,'Bon Secours Rappahannock General Hospital','Kilmarnock','VA'),(9,'Sentara Williamsburg Region Medical Center','Williamsburg','VA'),(10,'M.D. Express Grafton','Yorktown','VA'),(11,'M.D. Express','Williamsburg','VA'),(12,'M.D. Express','Newport News','VA'),(13,'M.D. Express','Hampton','VA'),(14,'Patient First Denbeigh','Newport News','VA'),(15,'TPMG (Tidewater Physicians Multispecialty Group)','Williamsburg','VA'),(16,'OSC (Orthopaedic and Spine Center)','Newport News (Port Warwick)','VA');
/*!40000 ALTER TABLE `ClinicalSite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'admin','$2y$10$mYtkU71AhpdY4mtUP6xnZOj2wp4Klo0r90vaHoLTmJRHvKCm6vQEa','admin@admin.com',1,0,0),(2,'clinician','$2y$10$xheH91bbwScEVYglo7I7eOw1bAPRF8e3IORIOAKV2af7Iie2zp0IK','clinic@clinic.com',0,1,0);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-09 13:46:24
